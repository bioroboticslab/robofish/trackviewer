# SPDX-License-Identifier: GPL-3.0-or-later

find_package(PkgConfig)
pkg_check_modules(PC_harfbuzz QUIET harfbuzz)

find_path(harfbuzz_INCLUDE_DIRS
    NAMES hb.h
    HINTS ${PC_harfbuzz_INCLUDEDIR}
          ${PC_harfbuzz_INCLUDE_DIRS}
    PATH_SUFFIXES harfbuzz
)

find_library(harfbuzz_LIBRARIES NAMES harfbuzz
    HINTS ${PC_harfbuzz_LIBDIR}
          ${PC_harfbuzz_LIBRARY_DIRS}
)

if(harfbuzz_INCLUDE_DIRS)
    if(EXISTS "${harfbuzz_INCLUDE_DIRS}/hb-version.h")
        file(READ "${harfbuzz_INCLUDE_DIRS}/hb-version.h" _harfbuzz_version_content)

        string(REGEX MATCH "#define +HB_VERSION_STRING +\"([0-9]+\\.[0-9]+\\.[0-9]+)\"" _dummy "${_harfbuzz_version_content}")
        set(harfbuzz_VERSION "${CMAKE_MATCH_1}")
    endif()
endif()

if("${harfbuzz_FIND_VERSION}" VERSION_GREATER "${harfbuzz_VERSION}")
    message(FATAL_ERROR "Required version(" ${harfbuzz_FIND_VERSION} ") is higher than found version(" ${harfbuzz_VERSION} ")")
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    harfbuzz
        REQUIRED_VARS harfbuzz_INCLUDE_DIRS harfbuzz_LIBRARIES
        VERSION_VAR harfbuzz_VERSION)

mark_as_advanced(
    harfbuzz_INCLUDE_DIRS
    harfbuzz_LIBRARIES
)

if(harfbuzz_FOUND)
    add_library(harfbuzz::harfbuzz UNKNOWN IMPORTED)
    set_target_properties(
        harfbuzz::harfbuzz PROPERTIES
            IMPORTED_LOCATION ${harfbuzz_LIBRARIES}
            INTERFACE_INCLUDE_DIRECTORIES "${harfbuzz_INCLUDE_DIRS}")
endif()
