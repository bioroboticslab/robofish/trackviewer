# SPDX-License-Identifier: GPL-3.0-or-later

find_package(PkgConfig)
pkg_check_modules(PC_Epoxy QUIET epoxy)
set(Epoxy_DEFINITIONS ${PC_Epoxy_CFLAGS_OTHER})

find_path(Epoxy_INCLUDE_DIR
   NAMES epoxy/gl.h
   HINTS ${PC_Epoxy_INCLUDEDIR} ${PC_Epoxy_INCLUDE_DIRS} $ENV{EPOXY_DIR}/include
   PATH_SUFFIXES libepoxy
)

if(PC_Epoxy_VERSION)
    set(Epoxy_VERSION "${PC_Epoxy_VERSION}")
endif()

if(Epoxy_FIND_VERSION)
    if("${Epoxy_FIND_VERSION}" VERSION_GREATER "${Epoxy_VERSION}")
        message(FATAL_ERROR "Required version " ${Epoxy_FIND_VERSION} " is higher than found version " ${Epoxy_VERSION})
    endif()
endif()

find_library(Epoxy_LIBRARY
   NAMES epoxy libepoxy
   HINTS ${PC_Epoxy_LIBDIR} ${PC_Epoxy_LIBRARY_DIRS} $ENV{EPOXY_DIR}/lib
)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(
   Epoxy
      REQUIRED_VARS Epoxy_LIBRARY Epoxy_INCLUDE_DIR
      VERSION_VAR Epoxy_VERSION
)

mark_as_advanced(Epoxy_INCLUDE_DIR Epoxy_LIBRARY)

if(Epoxy_FOUND)
   add_library(Epoxy UNKNOWN IMPORTED)
   set_target_properties(Epoxy PROPERTIES IMPORTED_LOCATION ${Epoxy_LIBRARY})
   set_target_properties(Epoxy PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${Epoxy_INCLUDE_DIR})
endif()
