# SPDX-License-Identifier: GPL-3.0-or-later

find_package(PkgConfig)
pkg_check_modules(PC_EGL QUIET egl)

find_path(EGL_INCLUDE_DIR
   NAMES EGL/egl.h
   HINTS ${PC_EGL_INCLUDEDIR} $ENV{EGL_DIR}/include
)

if(PC_EGL_VERSION)
    set(EGL_VERSION "${PC_EGL_VERSION}")
endif()

if(EGL_FIND_VERSION)
    if("${EGL_FIND_VERSION}" VERSION_GREATER "${EGL_VERSION}")
        message(FATAL_ERROR "Required version " ${EGL_FIND_VERSION} " is higher than found version " ${EGL_VERSION})
    endif()
endif()

find_library(EGL_LIBRARY
   NAMES EGL libEGL
   HINTS ${PC_EGL_LIBDIR} $ENV{EGL_DIR}/lib
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
   EGL
      REQUIRED_VARS EGL_LIBRARY EGL_INCLUDE_DIR
      VERSION_VAR EGL_VERSION
)

mark_as_advanced(EGL_INCLUDE_DIR EGL_LIBRARY)

if(EGL_FOUND)
   add_library(EGL UNKNOWN IMPORTED)
   set_target_properties(EGL PROPERTIES IMPORTED_LOCATION "${EGL_LIBRARY}")
   set_target_properties(EGL PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${EGL_INCLUDE_DIR}")
endif()
