# SPDX-License-Identifier: GPL-3.0-or-later

find_package(PkgConfig)
pkg_check_modules(PC_glm QUIET glm)

find_path(glm_INCLUDE_DIR
    NAMES glm/glm.hpp
    HINTS ${PC_glm_INCLUDEDIR}
)

if(PC_glm_VERSION)
    set(glm_VERSION "${PC_glm_VERSION}")
endif()

if(glm_FIND_VERSION)
    if("${glm_FIND_VERSION}" VERSION_GREATER "${glm_VERSION}")
        message(FATAL_ERROR "Required version(" ${glm_FIND_VERSION} ") is higher than found version(" ${glm_VERSION} ")")
    endif()
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    glm
        REQUIRED_VARS glm_INCLUDE_DIR
        VERSION_VAR glm_VERSION)

mark_as_advanced(
    glm_INCLUDE_DIR
)

if(glm_FOUND)
    add_library(glm::glm INTERFACE IMPORTED)
    set_target_properties(
        glm::glm PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${glm_INCLUDE_DIR}")
endif()
