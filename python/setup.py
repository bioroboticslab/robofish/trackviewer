# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
from subprocess import run, PIPE, check_call
from pathlib import Path

from setuptools import setup, find_packages, Extension
from setuptools.command.build_ext import build_ext


class CMakeExtension(Extension):
    def __init__(self, target, name, *, package=None, sourcedir):
        if package is None:
            module = name
        else:
            module = package + "." + name
        super().__init__(module, sources=[])
        self.target = target
        self.sourcedir = os.path.abspath(sourcedir)


class CMakeBuild(build_ext):
    def run(self):
        for ext in self.extensions:
            self.build_extension(ext)

    def build_extension(self, ext):
        env = os.environ.copy()
        extdir = os.path.abspath(os.path.dirname(self.get_ext_fullpath(ext.name)))
        cmake_args = [f"-DCMAKE_INSTALL_PREFIX={extdir}", f"-DPYTHON_EXECUTABLE={sys.executable}"]

        if "CMAKE_GENERATOR" in env:
            cmake_args += ["-G", env["CMAKE_GENERATOR"]]

        build_type = "Debug" if self.debug else "Release"
        cmake_args += [f"-DCMAKE_BUILD_TYPE={build_type}"]

        for var in [
            "CMAKE_PREFIX_PATH",
            "CMAKE_TOOLCHAIN_FILE",
            "VCPKG_TARGET_TRIPLET",
            "INSTALL_SHARED_LIBRARIES",
        ]:
            if var in env:
                cmake_args += [f"-D{var}={env[var]}"]
        cmake_args += [
            "-DCMAKE_SKIP_INSTALL_ALL_DEPENDENCY=ON",
            f"-Drobofish-trackviewer_DIR={(Path(ext.sourcedir) / '..' / '..' / 'build').resolve()}",
        ]

        build_dir = self.build_temp
        if not os.path.exists(build_dir):
            os.makedirs(build_dir)
        check_call(["cmake", ext.sourcedir] + cmake_args, cwd=build_dir, env=env)
        check_call(["cmake", "--build", ".", "--target", ext.target], cwd=build_dir, env=env)
        check_call(["cmake", "--build", ".", "--target", "install"], cwd=build_dir, env=env)


entry_points = {
    "gui_scripts": ["robofish-trackviewer=robofish.trackviewer.app:main"],
    "console_scripts": ["robofish-trackviewer-render=robofish.trackviewer.render_video:main"]
}

install_requires = ["numpy", "h5py", "robofish-core", "robofish-io", "PySide2>=5.15"]


def source_version():
    version_parts = (
        run(["git", "describe", "--tags", "--dirty"], check=True, stdout=PIPE, encoding="utf-8")
        .stdout.strip()
        .split("-")
    )

    if version_parts[-1] == "dirty":
        dirty = True
        version_parts = version_parts[:-1]
    else:
        dirty = False

    version = version_parts[0]
    if len(version_parts) == 3:
        version += ".post0"
        version += f".dev{version_parts[1]}+{version_parts[2]}"
        if dirty:
            version += ".dirty"
    elif dirty:
        version += "+dirty"

    return version


setup(
    name="robofish-trackviewer",
    version=source_version(),
    author="Moritz Maxeiner",
    author_email="moritz.maxeiner@fu-berlin.de",
    install_requires=install_requires,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    python_requires=">=3.7",
    packages=[f"robofish.{p}" for p in find_packages("src/robofish")],
    package_dir={"": "src"},
    ext_modules=[
        CMakeExtension("extension", "cpp", package="robofish.trackviewer", sourcedir="extension")
    ],
    entry_points=entry_points,
    cmdclass=dict(build_ext=CMakeBuild),
    zip_safe=False,
)
