#! /usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

from os import environ as env, pathsep
from subprocess import check_call
from pathlib import Path
from platform import system
from shutil import which


def python_executable():
    if system() == "Windows":
        return f"/Python{''.join(env['PYTHON_VERSION'].split('.'))}/python.exe"
    elif system() == "Linux" or system() == "Darwin":
        return which(f"python{env['PYTHON_VERSION']}")
    assert False


if __name__ == "__main__":
    if system() == "Windows":
        env["PATH"] += f"{pathsep}{env['VCPKG_INSTALL_DIR']}/bin"
        for path in Path("vendor").glob("*/bin"):
            env["PATH"] += f"{pathsep}{path}"

        shared_libraries = [
            "zlib1",
            "bz2",
            "libpng16",
            "brotlidec",
            "brotlicommon",
        ]
    elif system() == "Linux":
        shared_libraries = [
            "gmpxx.so.4",
            "mpfr.so.4",
        ]
    elif system() == "Darwin":
        shared_libraries = []
    else:
        assert False

    if len(shared_libraries) > 0:
        env["INSTALL_SHARED_LIBRARIES"] = ";".join(shared_libraries)

    env["CMAKE_PREFIX_PATH"] = str(Path("vendor").resolve())
    env["CMAKE_GENERATOR"] = "Ninja"

    command = [python_executable()]
    command += ["setup.py", "build"]

    check_call(command, cwd=Path("python").resolve())

    with open("build.env", "w") as f:
        f.write(f"PYTHON_VERSION={env['PYTHON_VERSION']}\n")
