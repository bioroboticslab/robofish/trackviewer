#! /usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

from os import environ as env
from subprocess import check_call
from pathlib import Path
from platform import system
from shutil import which


def python_executable():
    if system() == "Windows":
        return f"/Python{''.join(env['PYTHON_VERSION'].split('.'))}/python.exe"
    elif system() == "Linux" or system() == "Darwin":
        return which(f"python{env['PYTHON_VERSION']}")
    assert False


if __name__ == "__main__":
    command = [python_executable()]
    command += ["setup.py", "bdist_wheel", "--skip-build"]

    check_call(command, cwd=Path("python").resolve())

    with open("build.env", "w") as f:
        f.write(f"PYTHON_VERSION={env['PYTHON_VERSION']}\n")
