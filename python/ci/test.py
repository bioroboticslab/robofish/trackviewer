#! /usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

from os import environ as env, pathsep
from subprocess import check_call
from pathlib import Path
from platform import system
from shutil import which


def python_executable():
    if system() == "Windows":
        return f"/Python{''.join(env['PYTHON_VERSION'].split('.'))}/python.exe"
    elif system() == "Linux" or system() == "Darwin":
        return which(f"python{env['PYTHON_VERSION']}")
    assert False


def python_venv_executable():
    if system() == "Windows":
        return str(Path("python/.venv/Scripts/python.exe").resolve())
    elif system() == "Linux" or system() == "Darwin":
        return str(Path("python/.venv/bin/python").resolve())
    assert False


if __name__ == "__main__":
    check_call(
        [
            python_executable(),
            "-m",
            "venv",
            "--system-site-packages",
            "--prompt",
            "ci",
            ".venv",
        ],
        cwd=Path("python").resolve(),
    )

    check_call(
        [
            python_venv_executable(),
            "-m",
            "pip",
            "install",
            str(sorted(Path("python/dist").glob("*.whl"))[-1].resolve()),
        ],
        cwd=Path("python").resolve(),
    )

    check_call(
        [python_venv_executable(), "-m", "unittest", "discover", "-s", "tests"],
        cwd=Path("python").resolve(),
    )
