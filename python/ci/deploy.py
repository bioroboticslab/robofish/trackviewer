#! /usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

from os import environ as env
from subprocess import check_call
from pathlib import Path
from platform import system
from argparse import ArgumentParser


if __name__ == "__main__":
    if system() != "Linux":
        raise Exception("Uploading python package only supported on Linux")

    p = ArgumentParser()
    p.add_argument("--production", default=False, action="store_const", const=True)
    args = p.parse_args()

    env["TWINE_USERNAME"] = "gitlab-ci-token"
    env["TWINE_PASSWORD"] = env["CI_JOB_TOKEN"]

    if args.production:
        target_project_id = env['ARTIFACTS_REPOSITORY_PROJECT_ID']
    else:
        target_project_id = env['CI_PROJECT_ID']

    command = ["python3"]
    command += ["-m", "twine", "upload", "dist/*"]
    command += [
        "--repository-url",
        f"https://git.imp.fu-berlin.de/api/v4/projects/{target_project_id}/packages/pypi",
    ]

    check_call(command, cwd=Path("python").resolve())
