# SPDX-License-Identifier: GPL-3.0-or-later

import sys

assert (3, 7) <= sys.version_info < (4, 0), "Unsupported Python version"
