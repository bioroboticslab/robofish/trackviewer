# SPDX-License-Identifier: GPL-3.0-or-later

import sys
from argparse import ArgumentParser
from math import floor
from pathlib import Path
from subprocess import Popen, PIPE

from PySide2.QtCore import (
    Qt,
    QSize,
    Signal,
    QElapsedTimer,
    QRunnable,
    QThreadPool,
    QTimer,
    QObject,
)
from PySide2.QtGui import (
    QIcon,
    QSurfaceFormat,
    QOpenGLContext,
    QOpenGLVersionProfile,
    QKeySequence,
    QOffscreenSurface,
    QIntValidator,
)
from PySide2.QtWidgets import (
    QOpenGLWidget,
    QWidget,
    QApplication,
    QVBoxLayout,
    QHBoxLayout,
    QSlider,
    QSizePolicy,
    QStyle,
    QToolButton,
    QSpinBox,
    QProxyStyle,
    QFileDialog,
    QMainWindow,
    QAction,
    QProgressDialog,
    QLineEdit,
)

import h5py
from numpy import zeros, dtype

from robofish.trackviewer.cpp import SimpleFramebuffer, FrameGrabber
from robofish.trackviewer.common import RenderConfig, Renderer, ViewAction, TrackFile

defaults = {"width": 512, "height": 512, "far_plane": 144.0, "msaa_samples": 8}


class FrameSliderStyle(QProxyStyle):
    def styleHint(self, hint, option=None, widget=None, returnData=None):
        if hint == QStyle.SH_Slider_AbsoluteSetButtons:
            return Qt.LeftButton | Qt.MidButton | Qt.RightButton
        return super(FrameSliderStyle, self).styleHint(hint, option, widget, returnData)


class ExportVideo(QRunnable):
    class Signals(QObject):
        progressed = Signal(int)
        finished = Signal()

    def __init__(self, trackfile, filename, width, height, fps, ppi, samples, render_config):
        super().__init__()
        self.trackfile = trackfile
        self.filename = filename
        self.width = width
        self.height = height
        self.fps = fps
        self.ppi = ppi
        self.samples = samples
        self.renderer = Renderer(render_config)

        self.surface = QOffscreenSurface()
        self.surface.create()

        self.progress = QProgressDialog("Exporting video", "Cancel", 0, self.trackfile.num_frames)
        self.progress.setWindowModality(Qt.NonModal)
        self.progress.show()

        self.signals = ExportVideo.Signals()
        self.signals.progressed.connect(self.progress.setValue)
        self.signals.finished.connect(self.progress.close)

    def run(self):
        self.setAutoDelete(True)

        context = QOpenGLContext()
        context.create()
        context.makeCurrent(self.surface)

        framebuffer = SimpleFramebuffer(self.width, self.height, self.samples)
        self.renderer.set_ppi(self.ppi)
        self.renderer.set_data(self.trackfile)

        grabber = FrameGrabber(self.width, self.height)
        frame = zeros(grabber.frame_size, dtype=dtype("B"))

        encoder = Popen(
            [
                "ffmpeg",
                "-loglevel",
                "error",
                "-nostats",
                "-y",
                "-f",
                "rawvideo",
                "-c:v",
                "rawvideo",
                "-s",
                f"{self.width}x{self.height}",
                "-pix_fmt",
                "bgra",
                "-r",
                f"{self.fps}",
                "-i",
                "-",
                "-vf",
                "vflip",
                "-an",
                "-c:v",
                "h264",
                "-crf",
                "0",
                f"{self.filename}",
            ],
            stdin=PIPE,
        )

        num_frames = self.trackfile.num_frames

        for t in range(num_frames):
            self.signals.progressed.emit(t)
            if self.progress.wasCanceled():
                break
            framebuffer.bind()
            self.renderer.draw(self.width, self.height, t)
            grabber.grab(frame)
            encoder.stdin.write(frame)
        self.signals.progressed.emit(num_frames)
        encoder.stdin.close()
        encoder.wait()

        del grabber
        del self.renderer

        context.doneCurrent()
        self.signals.finished.emit()


class OpenGLTracksetView(QOpenGLWidget):
    def __init__(self, render_config):
        super().__init__()
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.frame = 0
        self.renderer = Renderer(render_config)

    def sizeHint(self):
        screen = self.window().windowHandle().screen()
        return QSize(
            defaults["width"] * screen.devicePixelRatio(),
            defaults["height"] * screen.devicePixelRatio(),
        )

    def initializeGL(self):
        vp = QOpenGLVersionProfile()
        vp.setVersion(4, 1)
        vp.setProfile(QSurfaceFormat.CoreProfile)
        self.gl = self.context().versionFunctions(vp)

        window = self.window().windowHandle()
        screen = window.screen()
        self.setScreen(screen)
        window.screenChanged.connect(self.setScreen)
        screen.logicalDotsPerInchChanged.connect(lambda _: self.setScreen(window.screen()))
        screen.physicalDotsPerInchChanged.connect(lambda _: self.setScreen(window.screen()))

    def setPixelsPerInch(self, value):
        self.makeCurrent()
        self.renderer.set_ppi(value)
        self.doneCurrent()
        self.update()

    def setScreen(self, screen):
        self.devicePixelRatio = screen.devicePixelRatio()
        self.setPixelsPerInch(float(screen.logicalDotsPerInch() * self.devicePixelRatio))

    @property
    def physicalWidth(self):
        return int(self.width() * self.devicePixelRatio)

    @property
    def physicalHeight(self):
        return int(self.height() * self.devicePixelRatio)

    def paintGL(self):
        self.renderer.draw(self.physicalWidth, self.physicalHeight, self.frame)

    def setFrame(self, frame):
        self.frame = frame
        self.update()

    def closeEvent(self, event):
        self.makeCurrent()
        del self.renderer
        self.doneCurrent()
        event.accept()

    def setTrackset(self, trackfile):
        self.renderer.set_data(trackfile)
        self.update()


class TrackViewer(QMainWindow):
    def __init__(self, render_config):
        super().__init__()
        self.setAttribute(Qt.WA_NativeWindow)

        self.setCentralWidget(QWidget())

        vbox = QVBoxLayout()
        vbox.setContentsMargins(0, 0, 0, 0)

        self.render_config = render_config
        trackset_view = OpenGLTracksetView(render_config)
        self.tracksetOpened.connect(trackset_view.setTrackset)
        vbox.addWidget(trackset_view)
        self.trackset_view = trackset_view

        playback_slider = QSlider(orientation=Qt.Horizontal)
        playback_slider.setStyle(FrameSliderStyle(self.style().objectName()))
        playback_slider.valueChanged.connect(trackset_view.setFrame)
        vbox.addWidget(playback_slider)
        self.playback_slider = playback_slider

        hbox = QHBoxLayout()
        hbox.setAlignment(Qt.AlignCenter)
        hbox.setContentsMargins(0, 0, 0, 0)

        self.play_icon = QIcon.fromTheme(
            "media-playback-start", self.style().standardIcon(QStyle.SP_MediaPlay)
        )
        self.pause_icon = QIcon.fromTheme(
            "media-playback-pause", self.style().standardIcon(QStyle.SP_MediaPause)
        )
        self.stop_icon = QIcon.fromTheme(
            "media-playback-stop", self.style().standardIcon(QStyle.SP_MediaStop)
        )

        playback_button = QToolButton()
        playback_button.setMinimumSize(QSize(16, 16))
        playback_button.setIconSize(QSize(16, 16))
        playback_button.setIcon(self.play_icon)
        playback_button.released.connect(self.togglePlayback)
        hbox.addWidget(playback_button)
        self.playback_button = playback_button

        stop_button = QToolButton()
        stop_button.setMinimumSize(QSize(16, 16))
        stop_button.setIconSize(QSize(16, 16))
        stop_button.setIcon(self.stop_icon)
        stop_button.released.connect(self.stopPlayback)
        hbox.addWidget(stop_button)

        fps_box = QSpinBox()
        fps_box.setMinimum(1)
        fps_box.setMaximum(10 ** 6)
        fps_box.setSingleStep(1)
        fps_box.hide()
        fps_box.valueChanged.connect(self.setFramesPerSecond)
        hbox.addWidget(fps_box)
        self.fps_box = fps_box

        frame_selector = QLineEdit()
        frame_selector.hide()
        frame_selector.editingFinished.connect(
            lambda: playback_slider.setValue(int(frame_selector.text()))
        )
        playback_slider.valueChanged.connect(lambda value: frame_selector.setText(str(value)))
        hbox.addWidget(frame_selector)
        self.frame_selector = frame_selector

        hbox.addStretch(1)
        vbox.addLayout(hbox)

        self.centralWidget().setLayout(vbox)

        file_menu = self.menuBar().addMenu(self.tr("&File"))

        open_trackset_action = QAction(self.tr("&Open..."), self)
        open_trackset_action.setShortcuts(QKeySequence.Open)
        open_trackset_action.triggered.connect(lambda checked: self.openTrackset())
        file_menu.addAction(open_trackset_action)

        export_video_action = QAction(self.tr("&Export Video..."), self)
        export_video_action.setEnabled(False)
        export_video_action.triggered.connect(lambda checked: self.exportVideo())
        file_menu.addAction(export_video_action)
        self.export_video_action = export_video_action

        save_screenshot_action = QAction(self.tr("&Save Screenshot..."), self)
        save_screenshot_action.setEnabled(False)
        save_screenshot_action.triggered.connect(lambda checked: self.saveScreenshot())
        file_menu.addAction(save_screenshot_action)
        self.save_screenshot_action = save_screenshot_action

        quit_action = QAction(self.tr("&Quit"), self)
        quit_action.setShortcuts(QKeySequence.Quit)
        quit_action.triggered.connect(self.close)
        file_menu.addAction(quit_action)

        self.playback_timer = QTimer()
        self.playback_timer.setTimerType(Qt.PreciseTimer)
        self.playback_timer.setSingleShot(False)
        self.playback_timer.timeout.connect(self.tick)
        self.elapsed_timer = QElapsedTimer()

        self.windowHandle().screenChanged.connect(self.on_screenChanged)

    def on_screenChanged(self, screen):
        self.playback_timer.setInterval(10 ** 3 / screen.refreshRate())

    def closeEvent(self, event):
        self.trackset_view.close()
        event.accept()

    tracksetOpened = Signal(TrackFile)

    def openTrackset(self, filename=None):
        if filename is None:
            filename, _ = QFileDialog.getOpenFileName(
                None, "Open TrackFile", ".", "TrackFile Files (*.hdf5)"
            )
        if filename:
            trackfile = TrackFile(filename)
            self.playback_slider.setValue(0)
            self.playback_slider.setRange(0, trackfile.num_frames - 1)
            self.fps_box.setValue(1000.0 / trackfile.time_step)
            self.fps_box.show()
            self.frame_selector.setText(str(0))
            self.frame_selector.show()
            self.frame_selector.setValidator(QIntValidator(0, trackfile.num_frames - 1))
            self.export_video_action.setEnabled(True)
            self.save_screenshot_action.setEnabled(True)
            self.tracksetOpened.emit(trackfile)
            self.trackfile = trackfile

    def togglePlayback(self):
        if not self.playback_timer.isActive():
            self.playback_timer.start()
            self.elapsed_timer.start()
            self.playback_button.setIcon(self.pause_icon)
        else:
            self.playback_timer.stop()
            self.playback_button.setIcon(self.play_icon)

    def stopPlayback(self):
        if self.playback_timer.isActive():
            self.playback_timer.stop()
            self.elapsed_timer.invalidate()
            self.playback_button.setIcon(self.play_icon)
        self.playback_slider.setValue(0)

    def setFramesPerSecond(self, value):
        self.playback_interval = 10 ** 3 / value

    def tick(self):
        if self.elapsed_timer.elapsed() < self.playback_interval:
            return
        num_frames = floor(self.elapsed_timer.restart() / self.playback_interval)
        frame = min(self.playback_slider.value() + num_frames, self.playback_slider.maximum())
        self.playback_slider.setValue(frame)
        if frame == self.playback_slider.maximum():
            self.playback_timer.stop()
            self.playback_button.setIcon(self.play_icon)

    def exportVideo(self, filename=None):
        if filename is None:
            filename, _ = QFileDialog.getSaveFileName(
                None, "Export Video", ".", "Video files (*.mp4)"
            )
        if filename:
            filename = Path(filename).with_suffix(".mp4")

            screen = self.windowHandle().screen()
            ppi = float(screen.logicalDotsPerInch() * screen.devicePixelRatio())
            QThreadPool.globalInstance().start(
                ExportVideo(
                    self.trackfile,
                    filename,
                    self.trackset_view.physicalWidth,
                    self.trackset_view.physicalHeight,
                    int(self.fps_box.value()),
                    ppi,
                    defaults["msaa_samples"],
                    self.render_config,
                )
            )

    def saveScreenshot(self, filename=None):
        if filename is None:
            filename, _ = QFileDialog.getSaveFileName(
                None, "Save screenshot", ".", "PNG files (*.png)"
            )
        if filename:
            filename = Path(filename).with_suffix(".png")
            self.trackset_view.grab().save(str(filename))


def main(args=None):
    def parse_args(args):
        p = ArgumentParser(description="View RoboFish tracks in a GUI.")
        p.add_argument(
            "trackset_file", nargs="?", help="Path to HDF5 file containing the tracks to view"
        )
        p.add_argument(
            "--draw-labels",
            action="store_true",
            help="Whether to draw labels inside the agents' outlines",
        )
        p.add_argument(
            "--draw-view-vectors",
            action="store_true",
            help="Whether to draw view vectors to the right of / below the trackfile",
        )
        p.add_argument(
            "--far-plane",
            default=defaults["far_plane"],
            type=float,
            help="Maximum distance an agent can see",
        )
        p.add_argument("--view-of-agents", action=ViewAction)
        p.add_argument("--view-of-walls", action=ViewAction)
        p.add_argument(
            "--view-of-walls-matches", action="store_const", dest="view_of_walls", const="match"
        )
        return p.parse_args(args)

    sf = QSurfaceFormat()
    sf.setVersion(4, 1)
    sf.setProfile(QSurfaceFormat.CoreProfile)
    sf.setSwapInterval(0)
    sf.setSwapBehavior(QSurfaceFormat.SingleBuffer)
    sf.setSamples(defaults["msaa_samples"])
    QSurfaceFormat.setDefaultFormat(sf)

    if args is None:
        args = sys.argv
    app = QApplication(args)
    app.setApplicationName("RoboFish TrackViewer")
    args = [str(arg) for arg in app.arguments()][1:]
    args = parse_args(args)

    viewer = TrackViewer(
        RenderConfig(
            args.draw_labels,
            args.draw_view_vectors,
            far_plane=args.far_plane,
            view_of_agents=args.view_of_agents,
            view_of_walls=args.view_of_walls,
        )
    )
    viewer.show()
    if args.trackset_file:
        viewer.openTrackset(args.trackset_file)
    return app.exec_()
