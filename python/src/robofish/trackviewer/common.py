# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import Action
from math import floor, log10

import numpy as np
from numpy import zeros, float32, linspace, radians, clip

import h5py

from robofish.core import entity_boundary_distance, entity_entities_minimum_sector_distances
import robofish.io

from robofish.trackviewer.cpp import (
    FrameRenderer,
    ViewVectorsRenderer,
)

def normalize(x, low, high):
    return clip((x - low) / (high - low), 0, 1)

def prepare_view_bins(view_of_agents, view_of_walls):
    if view_of_agents:
        fop, view_of_agents_size = view_of_agents
        view_of_agents_sectors = linspace(
            -radians(fop) / 2.0, radians(fop) / 2.0, view_of_agents_size + 1, dtype=float32
        )
    else:
        view_of_agents_size = 0
        view_of_agents_sectors = []

    if view_of_walls:
        fop, view_of_walls_size = view_of_walls
        view_of_walls_rays = linspace(
            -radians(fop) / 2.0, radians(fop) / 2.0, view_of_walls_size, dtype=float32
        )
    else:
        view_of_walls_size = 0
        view_of_walls_rays = []

    return (view_of_agents_size, view_of_agents_sectors), (view_of_walls_size, view_of_walls_rays)


def raycast_agent_centroids_and_walls(
    trackfile, view_of_agents, view_of_walls, far_plane, out, offset=0
):
    view_of_agents_size, view_of_agents_sectors = view_of_agents
    view_of_walls_size, view_of_walls_rays = view_of_walls

    boundary = float32([[-trackfile.world_size[0] / 2, -trackfile.world_size[1] / 2], [trackfile.world_size[0] / 2, trackfile.world_size[1] / 2]])

    for t in range(trackfile.num_frames):
        poses = []
        indices = {}
        for index, id_ in enumerate(trackfile.entity_names):
            poses.append(trackfile.frame(id_, t))
            indices[id_] = index
        poses = np.stack(poses)

        for id_ in trackfile.entity_names:
            if view_of_agents_size:
                view = out[id_][t][offset:][:view_of_agents_size]
                entity_entities_minimum_sector_distances(poses, indices[id_], view_of_agents_sectors, out=view)
                view[:] = 1 - normalize(view, 0, far_plane)

            if view_of_walls_size:
                view = out[id_][t][offset + view_of_agents_size :][:view_of_walls_size]
                entity_boundary_distance(poses[indices[id_]], boundary, view_of_walls_rays, out=view)
                view[:] = 1 - normalize(view, 0, far_plane)


def extract_view_vectors(trackfile, far_plane, view_of_agents, view_of_walls):
    if view_of_walls == "match" and view_of_agents:
        fop, n = view_of_agents
        view_of_walls = fop - fop / n, n
    view_of_agents, view_of_walls = prepare_view_bins(view_of_agents, view_of_walls)
    view_of_agents_size, _ = view_of_agents
    view_of_walls_size, _ = view_of_walls

    view_vectors = {
        id_: zeros((trackfile.num_frames, view_of_agents_size + view_of_walls_size), dtype=float32)
        for id_ in trackfile.entity_names
    }
    raycast_agent_centroids_and_walls(
        trackfile,
        view_of_agents,
        view_of_walls,
        far_plane,
        view_vectors,
        0,
    )
    return view_vectors


class RenderConfig:
    def __init__(
        self,
        draw_labels,
        draw_view_vectors,
        *,
        far_plane=142.0,
        view_of_agents=None,
        view_of_walls=None,
    ):
        self.draw_labels = draw_labels
        assert (
            not draw_view_vectors or view_of_agents is not None or view_of_walls is not None
        ), "view_of_agents or view_of_walls required"
        self.draw_view_vectors = draw_view_vectors
        self.far_plane = far_plane
        self.view_of_agents = view_of_agents
        self.view_of_walls = view_of_walls


class Renderer:
    def __init__(self, config):
        self.trackfile = None
        self.frame_renderer = None
        self.view_vectors = None
        self.view_vectors_renderer = None
        self.config = config

    def set_ppi(self, value):
        if self.frame_renderer is not None:
            del self.frame_renderer
        if self.view_vectors_renderer is not None:
            del self.view_vectors_renderer
        self.frame_renderer = FrameRenderer(value)
        self.view_vectors_renderer = ViewVectorsRenderer(value)

    def trackset_viewport(self, screen_width, screen_height):
        if self.trackfile is None:
            return None
        aspect_ratio = self.trackfile.world_size[0] / self.trackfile.world_size[1]
        height = screen_height
        width = height * aspect_ratio
        if width > screen_width:
            height *= screen_width / width
            width = screen_width
        return 0, screen_height - int(height), int(width), int(height)

    def view_vectors_viewport(self, screen_width, screen_height):
        trackset_viewport = self.trackset_viewport(screen_width, screen_height)
        free_width, free_height = (
            screen_width - trackset_viewport[2],
            screen_height - trackset_viewport[3],
        )
        if free_width > free_height:
            return (
                trackset_viewport[0] + trackset_viewport[2],
                trackset_viewport[1],
                free_width,
                trackset_viewport[3],
            )
        elif free_width < free_height:
            return (
                trackset_viewport[0],
                trackset_viewport[1] - free_height,
                trackset_viewport[2],
                free_height,
            )
        else:
            return None

    def draw(self, width, height, frame):
        if self.trackfile is None:
            return
        self.frame_renderer.draw(
            self.trackset_viewport(width, height),
            self.trackfile.world_size,
            list(self.trackfile.frame(id_, frame) for id_ in self.trackfile.entity_names),
            self.labels,
            self.agent_radius,
            0.05,
            0.5,
        )
        if self.view_vectors:
            view_vectors_viewport = self.view_vectors_viewport(width, height)
            if view_vectors_viewport is not None:
                self.view_vectors_renderer.draw(
                    view_vectors_viewport,
                    list(self.view_vectors[id][frame] for id in self.trackfile.entity_names),
                    0.05,
                    (3.0, 0.75),
                    (0.5, 0.5),
                    (0.5, 0.5),
                    self.labels,
                )

    def set_data(self, trackfile):
        self.trackfile = trackfile
        self.labels = (
            [] if not self.config.draw_labels else [str(id) for id in self.trackfile.entity_names]
        )

        if self.config.draw_view_vectors:
            self.view_vectors = extract_view_vectors(
                self.trackfile,
                self.config.far_plane,
                self.config.view_of_agents,
                self.config.view_of_walls,
            )

        if self.config.draw_labels:
            max_label_len = 1 + floor(log10(len(self.trackfile.entity_names)))
            self.agent_radius = 0.125 * max_label_len
        else:
            self.agent_radius = 0.1

    def __del__(self):
        if self.frame_renderer:
            del self.frame_renderer
        if self.view_vectors_renderer:
            del self.view_vectors_renderer


class ViewAction(Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        assert nargs is None
        super().__init__(
            option_strings, dest, 2, metavar=("field of perception", "number of bins"), **kwargs
        )

    def __call__(self, parser, namespace, values, option_string=None):
        items = (float32(values[0]), int(values[1]))
        setattr(namespace, self.dest, items)


class TrackFile(robofish.io.File):
    def __init__(self, file_name):
        super().__init__(file_name, "r")

        assert self.attrs["format_version"][0] == 1

        self._parse_samplings_and_entities()

    def _parse_samplings_and_entities(self):
        samplings = self["samplings"]

        assert len(samplings.values()) == 1, "Only a single sampling supported"

        assert "default" in samplings.attrs, "Default sampling required"
        default_sampling = samplings[samplings.attrs["default"]]

        assert "frequency_hz" in default_sampling.attrs, "Frequency required"
        self.time_step = 1000.0 / default_sampling.attrs["frequency_hz"]

        self.__entity_poses = {}
        for entity in self.entities:
            self.__entity_poses[entity.name] = entity.poses

        assert len(np.unique([p.shape for p in self.__entity_poses.values()], axis=0)) == 1, "All poses must have the same shape"
        assert len(self.__entity_poses) >= 1, "At least one pose required"
        self.num_frames = list(self.__entity_poses.values())[0].shape[0]


    def frame(self, entity_name, frame_index):
        return self.__entity_poses[entity_name][frame_index]
