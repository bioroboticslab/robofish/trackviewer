# SPDX-License-Identifier: GPL-3.0-or-later

import sys
from argparse import ArgumentParser
from pathlib import Path
import subprocess
import h5py
from numpy import zeros, dtype

from robofish.trackviewer.cpp import HeadlessContext, SimpleFramebuffer, FrameGrabber
from robofish.trackviewer.common import RenderConfig, Renderer, ViewAction, TrackFile

defaults = {"ppi": 96, "width": 512, "height": 512, "msaa_samples": 8, "far_plane": 144.0}


def run(
    trackfile,
    filename,
    render_config,
    *,
    width=defaults["width"],
    height=defaults["height"],
    ppi=defaults["ppi"],
    msaa_samples=defaults["msaa_samples"],
    ffmpeg=None,
    frames=None,
    fps=None,
):
    context = HeadlessContext()
    context.make_current()
    framebuffer = SimpleFramebuffer(width, height, msaa_samples)
    renderer = Renderer(render_config)
    renderer.set_ppi(ppi)
    renderer.set_data(trackfile)
    grabber = FrameGrabber(width, height)
    frame = zeros(grabber.frame_size, dtype=dtype("B"))
    if fps is None:
        fps = 1000.0 / trackfile.time_step
    ffmpeg = ffmpeg or "ffmpeg"

    encoder = subprocess.Popen(
        [
            ffmpeg,
            "-loglevel",
            "error",
            "-stats",
            "-y",
            "-f",
            "rawvideo",
            "-c:v",
            "rawvideo",
            "-s",
            f"{width}x{height}",
            "-pix_fmt",
            "bgra",
            "-r",
            f"{fps}",
            "-i",
            "-",
            "-vf",
            "vflip",
            "-an",
            "-c:v",
            "h264",
            "-pix_fmt",
            "yuv420p",
            "-crf",
            "17",
            f"{filename}",
        ],
        stdin=subprocess.PIPE,
    )

    for t in frames or range(trackfile.num_frames):
        framebuffer.bind()
        renderer.draw(width, height, t)
        grabber.grab(frame)
        encoder.stdin.write(frame)
    encoder.stdin.close()
    encoder.wait()
    del grabber
    del renderer
    del context


def main(args=None):
    def parse_args(args):
        p = ArgumentParser(description="Render RoboFish tracks to a video file.")
        p.add_argument(
            "trackset_file", type=Path, help="Path to HDF5 file containing the tracks to render"
        )

        p.add_argument("--ffmpeg", type=Path, help="Path to ffmpeg executable")

        o = p.add_mutually_exclusive_group()
        o.add_argument(
            "--output-directory",
            "--od",
            type=Path,
            help="Directory in which to store an automatically named video file",
        )
        o.add_argument(
            "-o", "--output-file", type=Path, help="Path to a manually named video file"
        )

        p.add_argument("--fps", type=float, help="Frames per second")
        p.add_argument("--width", default=defaults["width"], type=int, help="width of the video")
        p.add_argument(
            "--height", default=defaults["height"], type=int, help="height of the video"
        )
        p.add_argument(
            "--ppi", default=defaults["ppi"], type=int, help="pixels per inch in the video"
        )
        p.add_argument(
            "--msaa-samples",
            default=defaults["msaa_samples"],
            type=int,
            help="number of samples to use for multisample anti-aliasing",
        )
        p.add_argument("--frames", type=int, nargs=2, help="range of frames to render")
        p.add_argument(
            "--draw-labels",
            action="store_true",
            help="Whether to draw labels inside the agents' outlines",
        )
        p.add_argument(
            "--draw-view-vectors",
            action="store_true",
            help="Whether to draw view vectors to the right of / below the trackfile",
        )
        p.add_argument(
            "--far-plane",
            default=defaults["far_plane"],
            type=float,
            help="Maximum distance an agent can see",
        )
        p.add_argument("--view-of-agents", action=ViewAction)
        p.add_argument("--view-of-walls", action=ViewAction)
        p.add_argument(
            "--view-of-walls-matches", action="store_const", dest="view_of_walls", const="match"
        )
        return p.parse_args(args)

    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    if args.output_file:
        filename = args.output_file
    else:
        filename = (
            args.output_directory or args.trackset_file.parent
        ) / args.trackset_file.with_suffix(".mp4").name
    frames = (
        range(max(0, args.frames[0]), max(args.frames[0] + 1, args.frames[1]))
        if args.frames
        else None
    )

    trackfile = TrackFile(args.trackset_file)
    run(
        trackfile,
        filename,
        RenderConfig(
            args.draw_labels,
            args.draw_view_vectors,
            far_plane=args.far_plane,
            view_of_agents=args.view_of_agents,
            view_of_walls=args.view_of_walls,
        ),
        width=args.width,
        height=args.height,
        ppi=args.ppi,
        msaa_samples=args.msaa_samples,
        ffmpeg=args.ffmpeg,
        frames=frames,
        fps=args.fps,
    )
