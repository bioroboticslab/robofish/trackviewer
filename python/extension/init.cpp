// SPDX-License-Identifier: GPL-3.0-or-later

#include "python.hpp"

#include <robofish/trackviewer/render/SimpleFramebuffer.hpp>
#include <robofish/trackviewer/render/FrameRenderer.hpp>
#include <robofish/trackviewer/render/ViewVectorsRenderer.hpp>
#include <robofish/trackviewer/render/FrameGrabber.hpp>
#if WITH_EGL
#include <robofish/trackviewer/render/HeadlessContext.hpp>
#endif
#include <robofish/trackviewer/span.hpp>

PYBIND11_MODULE(cpp, cpp)
{
	using namespace pybind11;
	using namespace pybind11::literals;
	using namespace robofish::trackviewer;
	using namespace robofish;

#if WITH_EGL
	class_<HeadlessContext>(cpp, "HeadlessContext").def(init<>()).def("make_current", &HeadlessContext::make_current);
#endif

	class_<FrameRenderer>(cpp, "FrameRenderer")
	   .def(init<float>(), "ppi"_a)
	   .def(
	      "draw",
	      [](FrameRenderer&                             self,
	         std::tuple<GLint, GLint, GLsizei, GLsizei> viewport,
	         std::tuple<float, float>                   world,
	         std::vector<stx::span<const float, 4>>     agent_poses,
	         std::vector<std::string>                   agent_labels,
	         float                                      agent_radius,
	         float                                      agent_outline_thickness,
	         float                                      agent_tail_length) {
		      auto const [viewport_x, viewport_y, viewport_width, viewport_height] = viewport;
		      auto const [world_width, world_height]                               = world;
		      self.draw({viewport_x, viewport_y, viewport_width, viewport_height},
		                {world_width, world_height},
		                agent_poses,
		                agent_labels,
		                agent_radius,
		                agent_outline_thickness,
		                agent_tail_length);
	      },
	      "viewport"_a,
	      "world"_a,
	      "agent_poses"_a,
	      "agent_labels"_a,
	      "agent_radius"_a,
	      "agent_outline_thickness"_a,
	      "agent_tail_length"_a);

	class_<ViewVectorsRenderer>(cpp, "ViewVectorsRenderer")
	   .def(init<float>(), "ppi"_a)
	   .def(
	      "draw",
	      [](ViewVectorsRenderer&                       self,
	         std::tuple<GLint, GLint, GLsizei, GLsizei> viewport,
	         std::vector<stx::span<const float>>        view_vectors,
	         float                                      outline_thickness,
	         std::tuple<GLfloat, GLfloat>               size,
	         std::tuple<GLfloat, GLfloat>               margins,
	         std::tuple<GLfloat, GLfloat>               spacing,
	         std::vector<std::string>                   labels) {
		      auto const [viewport_x, viewport_y, viewport_width, viewport_height] = viewport;
		      auto const [size_x, size_y]                                          = size;
		      auto const [margins_x, margins_y]                                    = margins;
		      auto const [spacing_x, spacing_y]                                    = spacing;
		      self.draw({viewport_x, viewport_y, viewport_width, viewport_height},
		                view_vectors,
		                outline_thickness,
		                {size_x, size_y},
		                {margins_x, margins_y},
		                {spacing_x, spacing_y},
		                labels);
	      },
	      "viewport"_a,
	      "view_vectors"_a,
	      "outline_thickness"_a,
	      "size"_a,
	      "margins"_a,
	      "spacing"_a,
	      "labels"_a);

	class_<SimpleFramebuffer>(cpp, "SimpleFramebuffer")
	   .def(init<GLsizei, GLsizei, int>(), "width"_a, "height"_a, "samples"_a)
	   .def("bind", &SimpleFramebuffer::bind);

	class_<FrameGrabber>(cpp, "FrameGrabber")
	   .def(init<int, int>(), "width"_a, "height"_a)
	   .def_property_readonly("frame_size", &FrameGrabber::frame_size)
	   .def(
	      "grab",
	      [](FrameGrabber& self, array_t<unsigned char> frame) {
		      auto frame_ = frame.mutable_unchecked<1>();
		      self.grab(&frame_[0]);
	      },
	      "frame"_a);
}
