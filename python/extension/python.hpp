// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <robofish/trackviewer/span.hpp>

namespace pybind11::detail
{
	template<typename ElementType>
	struct type_caster<stx::span<ElementType, stx::dynamic_extent>>
	{
		using type       = stx::span<ElementType>;
		using array_type = array_t<ElementType, array::c_style | array::forcecast>;

		bool load(handle src, bool convert)
		{
			if (!array_type::check_(src))
				return false;
			auto array_value = array_type::ensure(src);
			if (!static_cast<bool>(array_value))
				return false;
			value = type{array_value.mutable_data(), static_cast<std::size_t>(array_value.size())};
			return true;
		}

		static handle cast(type src, return_value_policy policy, handle parent)
		{
			auto array_value = array_type(src.size(), src.data());
			return array_value.inc_ref();
		}

		PYBIND11_TYPE_CASTER(type, handle_type_name<array_type>::name);
	};

	template<typename ElementType, std::size_t Extent>
	struct type_caster<stx::span<ElementType, Extent>>
	{
		using type       = stx::span<ElementType, Extent>;
		using store_type = stx::span<ElementType, stx::dynamic_extent>;
		using array_type = array_t<ElementType, array::c_style | array::forcecast>;

		bool load(handle src, bool convert)
		{
			if (!array_type::check_(src))
				return false;
			auto array_value = array_type::ensure(src);
			if (!static_cast<bool>(array_value))
				return false;
			if (Extent != array_value.size())
				return false;
			value.emplace(array_value.mutable_data(), static_cast<std::size_t>(array_value.size()));
			return true;
		}

		static handle cast(type src, return_value_policy policy, handle parent)
		{
			auto array_value = array_type(src.size(), src.data());
			return array_value.inc_ref();
		}

	protected:
		std::optional<type> value;

	public:
		static constexpr auto name = _("numpy.ndarray[") + npy_format_descriptor<ElementType>::name + _(";") + _<Extent>()
		                             + _("]");
		template<typename T_, enable_if_t<std::is_same<type, remove_cv_t<T_>>::value, int> = 0>
		static handle cast(T_* src, return_value_policy policy, handle parent)
		{
			if (!src)
				return none().release();
			if (policy == return_value_policy::take_ownership) {
				auto h = cast(std::move(*src), policy, parent);
				delete src;
				return h;
			} else {
				return cast(*src, policy, parent);
			}
		}
		operator type*()
		{
			return &*value;
		}
		operator type&()
		{
			return *value;
		}
		operator type &&() &&
		{
			return std::move(*value);
		}
		template<typename T_>
		using cast_op_type = pybind11::detail::movable_cast_op_type<T_>;
	};
}
