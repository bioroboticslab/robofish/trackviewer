## Installation

See [common installation instructions](https://git.imp.fu-berlin.de/groups/bioroboticslab/robofish/-/wikis/Installation#python-packages).

## Usage

On Linux, macOS, and Windows:
```
$ robofish-trackviewer -h
usage: robofish-trackviewer [-h] [--draw-labels] [--draw-view-vectors]
                            [--far-plane FAR_PLANE]
                            [--view-of-agents field of perception number of bins]
                            [--view-of-walls field of perception number of bins]
                            [--view-of-walls-matches]
                            [trackset_file]

View RoboFish tracks in a GUI.

positional arguments:
  trackset_file         Path to HDF5 file containing the tracks to view

optional arguments:
  -h, --help            show this help message and exit
  --draw-labels         Whether to draw labels inside the agents' outlines
  --draw-view-vectors   Whether to draw view vectors to the right of / below
                        the trackfile
  --far-plane FAR_PLANE
                        Maximum distance an agent can see
  --view-of-agents field of perception number of bins
  --view-of-walls field of perception number of bins
  --view-of-walls-matches
```

On Linux:
```
$ robofish-trackviewer-render -h
usage: robofish-trackviewer-render [-h] [--ffmpeg FFMPEG]
                                   [--output-directory OUTPUT_DIRECTORY | -o OUTPUT_FILE]
                                   [--fps FPS] [--width WIDTH]
                                   [--height HEIGHT] [--ppi PPI]
                                   [--msaa-samples MSAA_SAMPLES]
                                   [--frames FRAMES FRAMES] [--draw-labels]
                                   [--draw-view-vectors]
                                   [--far-plane FAR_PLANE]
                                   [--view-of-agents field of perception number of bins]
                                   [--view-of-walls field of perception number of bins]
                                   [--view-of-walls-matches]
                                   trackset_file

Render RoboFish tracks to a video file.

positional arguments:
  trackset_file         Path to HDF5 file containing the tracks to render

optional arguments:
  -h, --help            show this help message and exit
  --ffmpeg FFMPEG       Path to ffmpeg executable
  --output-directory OUTPUT_DIRECTORY, --od OUTPUT_DIRECTORY
                        Directory in which to store an automatically named
                        video file
  -o OUTPUT_FILE, --output-file OUTPUT_FILE
                        Path to a manually named video file
  --fps FPS             Frames per second
  --width WIDTH         width of the video
  --height HEIGHT       height of the video
  --ppi PPI             pixels per inch in the video
  --msaa-samples MSAA_SAMPLES
                        number of samples to use for multisample anti-aliasing
  --frames FRAMES FRAMES
                        range of frames to render
  --draw-labels         Whether to draw labels inside the agents' outlines
  --draw-view-vectors   Whether to draw view vectors to the right of / below
                        the trackfile
  --far-plane FAR_PLANE
                        Maximum distance an agent can see
  --view-of-agents field of perception number of bins
  --view-of-walls field of perception number of bins
  --view-of-walls-matches
```
