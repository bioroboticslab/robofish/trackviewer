// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <cstddef>
#include <cassert>
#include <type_traits>
#include <iterator>
#include <array>
#include <algorithm>

namespace stx
{
	inline constexpr std::size_t dynamic_extent = -1;
	template<typename ElementType, std::size_t Extent = dynamic_extent>
	class span;

	namespace detail
	{
		template<typename T>
		struct is_span_impl : public std::false_type
		{
		};

		template<typename T, std::size_t Extent>
		struct is_span_impl<span<T, Extent>> : public std::true_type
		{
		};

		template<typename T>
		struct is_span : public is_span_impl<std::remove_cv_t<T>>
		{
		};

		template<typename T>
		struct is_std_array_impl : public std::false_type
		{
		};

		template<typename T, std::size_t N>
		struct is_std_array_impl<std::array<T, N>> : public std::true_type
		{
		};

		template<typename T>
		struct is_std_array : public is_std_array_impl<std::remove_cv_t<T>>
		{
		};

		template<typename T, class ElementType>
		struct is_span_compatible_ptr : public std::bool_constant<std::is_convertible_v<T (*)[], ElementType (*)[]>>
		{
		};

		template<typename T, class ElementType, class = void>
		struct is_span_compatible_container : public std::false_type
		{
		};

		template<typename T, class ElementType>
		struct is_span_compatible_container<
		   T,
		   ElementType,
		   std::void_t<
		      std::enable_if_t<!is_span<T>::value, std::nullptr_t>,
		      std::enable_if_t<!is_std_array<T>::value, std::nullptr_t>,
		      decltype(std::data(std::declval<T>())),
		      decltype(std::size(std::declval<T>())),
		      std::enable_if_t<is_span_compatible_ptr<std::remove_pointer_t<decltype(std::data(std::declval<T&>()))>,
		                                              ElementType>::value,
		                       std::nullptr_t>>> : public std::true_type
		{
		};
	}

	template<typename ElementType, std::size_t Extent>
	class span
	{
	public:
		using element_type           = ElementType;
		using value_type             = std::remove_cv_t<ElementType>;
		using index_type             = std::size_t;
		using difference_type        = std::size_t;
		using pointer                = ElementType*;
		using reference              = ElementType&;
		using iterator               = pointer;
		using const_iterator         = const ElementType*;
		using reverse_iterator       = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		static constexpr index_type extent = Extent;

		constexpr span() noexcept
		: data_m{nullptr}
		{
			static_assert(Extent == 0);
		}

		constexpr span(pointer ptr, [[maybe_unused]] index_type count)
		: data_m{ptr}
		{
			assert(Extent == count);
		}

		constexpr span(pointer f, pointer l)
		: data_m{f}
		{
			assert(Extent == std::distance(f, l));
		}

		constexpr span(element_type (&arr)[Extent]) noexcept
		: data_m{arr}
		{
		}

		constexpr span(std::array<value_type, Extent>& arr) noexcept
		: data_m{arr.data()}
		{
		}

		constexpr span(const std::array<value_type, Extent>& arr) noexcept
		: data_m{arr.data()}
		{
		}

		template<typename _Container>
		constexpr span(_Container& c,
		               const std::enable_if_t<detail::is_span_compatible_container<_Container, ElementType>::value,
		                                      std::nullptr_t> = nullptr)
		: data_m{std::data(c)}
		{
			assert(Extent == std::size(c));
		}

		template<typename _Container>
		constexpr span(const _Container& c,
		               const std::enable_if_t<detail::is_span_compatible_container<const _Container, ElementType>::value,
		                                      std::nullptr_t> = nullptr)
		: data_m{std::data(c)}
		{
			assert(Extent == std::size(c));
		}

		template<typename _OtherElementType, std::size_t _OtherExtent>
		constexpr span(const span<_OtherElementType, _OtherExtent>& other,
		               const std::enable_if_t<std::is_convertible_v<_OtherElementType (*)[], element_type (*)[]>,
		                                      std::nullptr_t> = nullptr) noexcept
		: data_m{other.data()}
		{
			static_assert(Extent == _OtherExtent || _OtherExtent == dynamic_extent);
			assert(Extent == other.size());
		}

		constexpr span(const span& rhs) noexcept = default;

		constexpr span& operator=(const span& rhs) noexcept = default;

		~span() noexcept = default;

		template<std::size_t count>
		constexpr auto first() const -> span<element_type, count>
		{
			assert(count <= size());
			return {data(), count};
		}

		template<std::size_t count>
		constexpr auto last() const -> span<element_type, count>
		{
			assert(count <= size());
			return {data() + size() - count, count};
		}

		constexpr auto first(index_type count) const -> span<element_type, dynamic_extent>
		{
			assert(count <= size());
			return {data(), count};
		}

		constexpr auto last(index_type count) const -> span<element_type, dynamic_extent>
		{
			assert(count <= size());
			return {data() + size() - count, count};
		}

		template<std::size_t offset, std::size_t count = dynamic_extent>
		constexpr auto subspan() const -> span<element_type, (count != dynamic_extent ? count : Extent - offset)>
		{
			assert(offset >= 0 && offset <= size());
			return {data() + offset, count == dynamic_extent ? size() - offset : count};
		}

		constexpr auto subspan(index_type offset, index_type count = dynamic_extent) const
		   -> span<element_type, dynamic_extent>
		{
			assert(offset <= size());
			assert(count <= size() || count == dynamic_extent);
			if (count == dynamic_extent)
				return {data() + offset, size() - offset};
			assert(offset + count <= size());
			return {data() + offset, count};
		}

		constexpr auto size() const noexcept -> index_type
		{
			return Extent;
		}
		constexpr auto size_bytes() const noexcept -> index_type
		{
			return Extent * sizeof(element_type);
		}
		constexpr auto empty() const noexcept -> bool
		{
			return Extent == 0;
		}

		constexpr auto operator[](index_type idx) const -> reference
		{
			return data_m[idx];
		}
		constexpr auto operator()(index_type idx) const -> reference
		{
			return data_m[idx];
		}
		constexpr auto data() const noexcept -> pointer
		{
			return data_m;
		}

		constexpr auto begin() const noexcept -> iterator
		{
			return data();
		}
		constexpr auto end() const noexcept -> iterator
		{
			return data() + size();
		}
		constexpr auto cbegin() const noexcept -> const_iterator
		{
			return data();
		}
		constexpr auto cend() const noexcept -> const_iterator
		{
			return data() + size();
		}
		constexpr auto rbegin() const noexcept -> reverse_iterator
		{
			return reverse_iterator(end());
		}
		constexpr auto rend() const noexcept -> reverse_iterator
		{
			return reverse_iterator(begin());
		}
		constexpr auto crbegin() const noexcept -> const_reverse_iterator
		{
			return const_reverse_iterator(cend());
		}
		constexpr auto crend() const noexcept -> const_reverse_iterator
		{
			return const_reverse_iterator(cbegin());
		}

		constexpr void swap(span& other) noexcept
		{
			pointer p    = data_m;
			data_m       = other.data_m;
			other.data_m = p;
		}

	private:
		pointer data_m;
	};

	template<typename ElementType>
	class span<ElementType, dynamic_extent>
	{
	private:
	public:
		using element_type           = ElementType;
		using value_type             = std::remove_cv_t<ElementType>;
		using index_type             = std::size_t;
		using difference_type        = std::size_t;
		using pointer                = ElementType*;
		using reference              = ElementType&;
		using iterator               = pointer;
		using const_iterator         = const ElementType*;
		using reverse_iterator       = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		static constexpr index_type extent = dynamic_extent;

		constexpr span() noexcept
		: data_m{nullptr}
		, size_m{0}
		{
		}

		constexpr span(pointer ptr, index_type count)
		: data_m{ptr}
		, size_m{count}
		{
		}

		constexpr span(pointer f, pointer l)
		: data_m{f}
		, size_m{std::distance(f, l)}
		{
		}

		template<std::size_t N>
		constexpr span(element_type (&arr)[N]) noexcept
		: data_m{arr}
		, size_m{N}
		{
		}

		template<std::size_t N>
		constexpr span(std::array<value_type, N>& arr) noexcept
		: data_m{arr.data()}
		, size_m{N}
		{
		}

		template<std::size_t N>
		constexpr span(const std::array<value_type, N>& arr) noexcept
		: data_m{arr.data()}
		, size_m{N}
		{
		}

		template<typename _Container>
		constexpr span(_Container& c,
		               const std::enable_if_t<detail::is_span_compatible_container<_Container, ElementType>::value,
		                                      std::nullptr_t> = nullptr)
		: data_m{std::data(c)}
		, size_m{(index_type) std::size(c)}
		{
		}

		template<typename _Container>
		constexpr span(const _Container& c,
		               const std::enable_if_t<detail::is_span_compatible_container<const _Container, ElementType>::value,
		                                      std::nullptr_t> = nullptr)
		: data_m{std::data(c)}
		, size_m{(index_type) std::size(c)}
		{
		}

		template<typename _OtherElementType, std::size_t _OtherExtent>
		constexpr span(const span<_OtherElementType, _OtherExtent>& other,
		               const std::enable_if_t<std::is_convertible_v<_OtherElementType (*)[], element_type (*)[]>,
		                                      std::nullptr_t> = nullptr)
		: data_m{other.data()}
		, size_m{other.size()}
		{
		}

		constexpr span(const span& rhs) noexcept = default;

		constexpr span& operator=(const span& rhs) noexcept = default;

		~span() noexcept = default;

		template<std::size_t count>
		constexpr auto first() const -> span<element_type, count>
		{
			assert(count <= size());
			return {data(), count};
		}

		template<std::size_t count>
		constexpr auto last() const -> span<element_type, count>
		{
			assert(count <= size());
			return {data() + size() - count, count};
		}

		constexpr auto first(index_type count) const -> span<element_type, dynamic_extent>
		{
			assert(count <= size());
			return {data(), count};
		}

		constexpr auto last(index_type count) const -> span<element_type, dynamic_extent>
		{
			assert(count <= size());
			return {data() + size() - count, count};
		}

		template<std::size_t offset, std::size_t count = dynamic_extent>
		constexpr auto subspan() const -> span<ElementType, dynamic_extent>
		{
			assert(offset <= size());
			assert(count == dynamic_extent || offset + count <= size());
			return {data() + offset, count == dynamic_extent ? size() - offset : count};
		}

		constexpr auto subspan(index_type offset, index_type count = dynamic_extent) const
		   -> span<element_type, dynamic_extent>
		{
			assert(offset <= size());
			assert(count <= size() || count == dynamic_extent);
			if (count == dynamic_extent)
				return {data() + offset, size() - offset};
			assert(offset + count <= size());
			return {data() + offset, count};
		}

		constexpr auto size() const noexcept -> index_type
		{
			return size_m;
		}
		constexpr auto size_bytes() const noexcept -> index_type
		{
			return size_m * sizeof(element_type);
		}
		constexpr auto empty() const noexcept -> bool
		{
			return size_m == 0;
		}

		constexpr auto operator[](index_type idx) const -> reference
		{
			return data_m[idx];
		}
		constexpr auto operator()(index_type idx) const -> reference
		{
			return data_m[idx];
		}
		constexpr auto data() const noexcept -> pointer
		{
			return data_m;
		}

		constexpr auto begin() const noexcept -> iterator
		{
			return data();
		}
		constexpr auto end() const noexcept -> iterator
		{
			return data() + size();
		}
		constexpr auto cbegin() const noexcept -> const_iterator
		{
			return data();
		}
		constexpr auto cend() const noexcept -> const_iterator
		{
			return data() + size();
		}
		constexpr auto rbegin() const noexcept -> reverse_iterator
		{
			return reverse_iterator(end());
		}
		constexpr auto rend() const noexcept -> reverse_iterator
		{
			return reverse_iterator(begin());
		}
		constexpr auto crbegin() const noexcept -> const_reverse_iterator
		{
			return const_reverse_iterator(cend());
		}
		constexpr auto crend() const noexcept -> const_reverse_iterator
		{
			return const_reverse_iterator(cbegin());
		}

		constexpr void swap(span& other) noexcept
		{
			pointer p    = data_m;
			data_m       = other.data_m;
			other.data_m = p;

			index_type sz = size_m;
			size_m        = other.size_m;
			other.size_m  = sz;
		}

	private:
		pointer    data_m;
		index_type size_m;
	};

	namespace detail
	{
		template<typename T1, typename T2, typename = void>
		struct is_equality_comparable : public std::false_type
		{
		};

		template<typename T1, typename T2>
		struct is_equality_comparable<
		   T1,
		   T2,
		   std::void_t<std::enable_if_t<
		                  std::is_convertible_v<bool, decltype(std::declval<const T1&>() == std::declval<const T2&>())>>,
		               std::nullptr_t>> : public std::true_type
		{
		};

		template<typename T1, typename T2, typename = void>
		struct is_less_than_comparable : public std::false_type
		{
		};

		template<typename T1, typename T2>
		struct is_less_than_comparable<
		   T1,
		   T2,
		   std::void_t<std::enable_if_t<
		                  std::is_convertible_v<bool, decltype(std::declval<const T1&>() < std::declval<const T2&>())>>,
		               std::nullptr_t>> : public std::true_type
		{
		};
	}

	template<typename ElementType1, std::size_t Extent1, typename ElementType2, std::size_t Extent2>
	constexpr auto operator==(const span<ElementType1, Extent1>& lhs, const span<ElementType2, Extent2>& rhs)
	   -> std::enable_if_t<detail::is_equality_comparable<ElementType1, ElementType2>::value, bool>
	{
		return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
	}

	template<typename ElementType1, std::size_t Extent1, typename ElementType2, std::size_t Extent2>
	constexpr auto operator!=(const span<ElementType1, Extent1>& lhs, const span<ElementType2, Extent2>& rhs)
	   -> std::enable_if_t<detail::is_equality_comparable<ElementType1, ElementType2>::value, bool>
	{
		return !(rhs == lhs);
	}

	template<typename ElementType1, std::size_t Extent1, typename ElementType2, std::size_t Extent2>
	constexpr auto operator<(const span<ElementType1, Extent1>& lhs, const span<ElementType2, Extent2>& rhs)
	   -> std::enable_if_t<detail::is_less_than_comparable<ElementType1, ElementType2>::value, bool>
	{
		return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
	}

	template<typename ElementType1, std::size_t Extent1, typename ElementType2, std::size_t Extent2>
	constexpr auto operator<=(const span<ElementType1, Extent1>& lhs, const span<ElementType2, Extent2>& rhs)
	   -> std::enable_if_t<detail::is_less_than_comparable<ElementType2, ElementType1>::value, bool>
	{
		return !(rhs < lhs);
	}

	template<typename ElementType1, std::size_t Extent1, typename ElementType2, std::size_t Extent2>
	constexpr auto operator>(const span<ElementType1, Extent1>& lhs, const span<ElementType2, Extent2>& rhs)
	   -> std::enable_if_t<detail::is_less_than_comparable<ElementType2, ElementType1>::value, bool>
	{
		return rhs < lhs;
	}

	template<typename ElementType1, std::size_t Extent1, typename ElementType2, std::size_t Extent2>
	constexpr auto operator>=(const span<ElementType1, Extent1>& lhs, const span<ElementType2, Extent2>& rhs)
	   -> std::enable_if_t<detail::is_less_than_comparable<ElementType1, ElementType2>::value, bool>
	{
		return !(lhs < rhs);
	}

	template<typename ElementType, std::size_t Extent>
	auto as_bytes(span<ElementType, Extent> s) noexcept
	{
		if constexpr (Extent == dynamic_extent)
			return span<const std::byte, dynamic_extent>{reinterpret_cast<const std::byte*>(s.data()), s.size_bytes()};
		else
			return span<const std::byte, Extent * sizeof(ElementType)>{reinterpret_cast<const std::byte*>(s.data()),
			                                                           s.size_bytes()};
	}

	template<typename ElementType,
	         std::size_t Extent,
	         std::enable_if_t<!std::is_const_v<ElementType>, std::nullptr_t> = nullptr>
	auto as_writable_bytes(span<ElementType, Extent> s) noexcept
	{
		if constexpr (Extent == dynamic_extent)
			return span<const std::byte, dynamic_extent>{reinterpret_cast<std::byte*>(s.data()), s.size_bytes()};
		else
			return span<const std::byte, Extent * sizeof(ElementType)>{reinterpret_cast<std::byte*>(s.data()),
			                                                           s.size_bytes()};
	}

	template<typename ElementType, std::size_t N>
	span(ElementType (&)[N])->span<ElementType, N>;

	template<typename ElementType, std::size_t N>
	span(std::array<ElementType, N>&)->span<ElementType, N>;

	template<typename ElementType, std::size_t N>
	span(const std::array<ElementType, N>&)->span<const ElementType, N>;

	template<typename _Container>
	span(_Container&)->span<typename _Container::value_type>;

	template<typename _Container>
	span(const _Container&)->span<const typename _Container::value_type>;
}

namespace std
{
	template<typename ElementType, std::size_t Extent>
	constexpr void swap(stx::span<ElementType, Extent>& lhs, stx::span<ElementType, Extent>& rhs) noexcept
	{
		lhs.swap(rhs);
	}
}
