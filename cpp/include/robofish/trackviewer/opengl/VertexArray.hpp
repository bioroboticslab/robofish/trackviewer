// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <epoxy/gl.h>

namespace robofish::trackviewer
{
	struct VertexArray
	{
		GLuint handle;
		       operator GLuint() const
		{
			return handle;
		}

		VertexArray(VertexArray const& src) = delete;
		VertexArray(VertexArray&& src)      = delete;
		VertexArray& operator=(VertexArray const& rhs) = delete;
		VertexArray& operator=(VertexArray&& rhs) = delete;

		VertexArray()
		{
			glGenVertexArrays(1, &handle);
		}

		~VertexArray()
		{
			glDeleteVertexArrays(1, &handle);
		}
	};
}
