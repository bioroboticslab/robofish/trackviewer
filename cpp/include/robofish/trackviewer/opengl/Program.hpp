// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <functional>

#include <epoxy/gl.h>

#include <robofish/trackviewer/opengl/Shader.hpp>

namespace robofish::trackviewer
{
	struct Program
	{
		GLuint handle;
		       operator GLuint() const
		{
			return handle;
		}

		Program(Program const& src) = delete;
		Program(Program&& src)      = delete;
		Program& operator=(Program const& rhs) = delete;
		Program& operator=(Program&& rhs) = delete;

		template<typename... List>
		Program(List&&... shaders)
		: Program(std::initializer_list<std::reference_wrapper<const Shader>>{std::cref(shaders)...})
		{
		}

		template<typename List>
		Program(List&& shaders)
		{
			handle = glCreateProgram();

			for (decltype(auto) shader : shaders) {
				glAttachShader(handle, [](Shader const& shader) { return shader.handle; }(shader));
			}

			glLinkProgram(handle);

			for (decltype(auto) shader : shaders) {
				glDetachShader(handle, [](Shader const& shader) { return shader.handle; }(shader));
			}

			GLint is_linked = false;
			glGetProgramiv(handle, GL_LINK_STATUS, &is_linked);
			if (!is_linked) {
				GLint log_len = 0;
				glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &log_len);

				std::vector<GLchar> log(log_len);
				glGetProgramInfoLog(handle, log_len, &log_len, &log[0]);

				glDeleteProgram(handle);

				throw std::runtime_error(&log[0]);
			}
		}

		~Program()
		{
			glDeleteProgram(handle);
		}
	};
}
