// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <epoxy/gl.h>

namespace robofish::trackviewer
{
	struct Buffer
	{
		GLuint handle;
		       operator GLuint() const
		{
			return handle;
		}

		Buffer(Buffer const& src) = delete;
		Buffer(Buffer&& src)      = delete;
		Buffer& operator=(Buffer const& rhs) = delete;
		Buffer& operator=(Buffer&& rhs) = delete;

		Buffer()
		{
			glGenBuffers(1, &handle);
		}

		~Buffer()
		{
			glDeleteBuffers(1, &handle);
		}
	};
}
