// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <epoxy/gl.h>

namespace robofish::trackviewer
{
	struct Renderbuffer
	{
		GLuint handle;
		       operator GLuint() const
		{
			return handle;
		}

		Renderbuffer(Renderbuffer const& src) = delete;
		Renderbuffer(Renderbuffer&& src)      = delete;
		Renderbuffer& operator=(Renderbuffer const& rhs) = delete;
		Renderbuffer& operator=(Renderbuffer&& rhs) = delete;

		Renderbuffer()
		{
			glGenRenderbuffers(1, &handle);
		}

		~Renderbuffer()
		{
			glDeleteRenderbuffers(1, &handle);
		}
	};
}
