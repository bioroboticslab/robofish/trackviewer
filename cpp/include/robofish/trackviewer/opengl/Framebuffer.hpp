// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <epoxy/gl.h>

namespace robofish::trackviewer
{
	struct Framebuffer
	{
		GLuint handle;
		       operator GLuint() const
		{
			return handle;
		}

		Framebuffer(Framebuffer const& src) = delete;
		Framebuffer(Framebuffer&& src)      = delete;
		Framebuffer& operator=(Framebuffer const& rhs) = delete;
		Framebuffer& operator=(Framebuffer&& rhs) = delete;

		Framebuffer()
		{
			glGenFramebuffers(1, &handle);
		}

		~Framebuffer()
		{
			glDeleteFramebuffers(1, &handle);
		}
	};
}
