// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <vector>
#include <stdexcept>

#include <epoxy/gl.h>

namespace robofish::trackviewer
{
	struct Shader
	{
		GLuint handle;
		       operator GLuint() const
		{
			return handle;
		}

		Shader(Shader const& src) = delete;
		Shader(Shader&& src)      = delete;
		Shader& operator=(Shader const& rhs) = delete;
		Shader& operator=(Shader&& rhs) = delete;

		template<typename Array>
		Shader(GLenum type, Array const& source)
		{
			handle = glCreateShader(type);

			auto src_list = reinterpret_cast<GLchar const*>(&source[0]);
			auto len_list = static_cast<GLint>(source.size());

			glShaderSource(handle, 1, &src_list, &len_list);

			glCompileShader(handle);
			GLint is_compiled = false;
			glGetShaderiv(handle, GL_COMPILE_STATUS, &is_compiled);

			if (!is_compiled) {
				GLint log_len = 0;
				glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &log_len);

				std::vector<GLchar> log(log_len);
				glGetShaderInfoLog(handle, log_len, &log_len, &log[0]);

				glDeleteShader(handle);

				throw std::runtime_error(&log[0]);
			}
		}

		~Shader()
		{
			glDeleteShader(handle);
		}
	};
}
