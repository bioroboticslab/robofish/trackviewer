// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <epoxy/gl.h>

namespace robofish::trackviewer
{
	struct Texture
	{
		GLuint handle;
		       operator GLuint() const
		{
			return handle;
		}

		Texture(Texture const& src) = delete;
		Texture(Texture&& src)      = delete;
		Texture& operator=(Texture const& rhs) = delete;
		Texture& operator=(Texture&& rhs) = delete;

		Texture(GLenum target)
		{
			glGenTextures(1, &handle);
			glBindTexture(target, handle);
			glBindTexture(target, 0);
		}

		~Texture()
		{
			glDeleteTextures(1, &handle);
		}
	};
}
