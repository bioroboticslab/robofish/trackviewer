// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>
#include <stdexcept>

#include <epoxy/gl.h>

#include <robofish/trackviewer/opengl/Framebuffer.hpp>
#include <robofish/trackviewer/opengl/Renderbuffer.hpp>

namespace robofish::trackviewer
{
	class SimpleFramebuffer : public Framebuffer
	{
		std::array<Renderbuffer, 2> renderbuffers_m;

	public:
		SimpleFramebuffer(GLsizei width, GLsizei height, int samples)
		: Framebuffer()
		{
			if (samples > 1) {
				glBindRenderbuffer(GL_RENDERBUFFER, renderbuffers_m[0]);
				glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_RGBA8, width, height);
				glBindRenderbuffer(GL_RENDERBUFFER, renderbuffers_m[1]);
				glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH_COMPONENT16, width, height);
			} else {
				glBindRenderbuffer(GL_RENDERBUFFER, renderbuffers_m[0]);
				glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, width, height);
				glBindRenderbuffer(GL_RENDERBUFFER, renderbuffers_m[1]);
				glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
			}

			glBindFramebuffer(GL_FRAMEBUFFER, *this);

			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, renderbuffers_m[0]);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderbuffers_m[1]);

			if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
				throw std::runtime_error("OpenGL Error: Failed to setup framebuffer, try requesting "
				                         "fewer samples");
			}

			glDrawBuffer(GL_COLOR_ATTACHMENT0);
		}

		void bind()
		{
			glBindFramebuffer(GL_FRAMEBUFFER, *this);
		}
	};
}
