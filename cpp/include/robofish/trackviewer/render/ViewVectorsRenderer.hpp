// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

#include <robofish/trackviewer/span.hpp>

#include <robofish/trackviewer/render/QuadRenderer.hpp>
#include <robofish/trackviewer/render/RectangleRenderer.hpp>
#include <robofish/trackviewer/render/TextRenderer.hpp>

namespace robofish::trackviewer
{
	class ViewVectorsRenderer
	{
		float             ppcm_m;
		QuadRenderer      quad_renderer_m;
		RectangleRenderer rectangle_renderer_m;
		TextRenderer      text_renderer_m;

	public:
		ViewVectorsRenderer(float ppi);

		void setPixelsPerInch(float ppi);

		void draw(glm::uvec4                        viewport,
		          stx::span<stx::span<const float>> view_vectors,
		          float                             outline_thickness,
		          glm::vec2                         size,
		          glm::vec2                         margins,
		          glm::vec2                         spacing,
		          stx::span<std::string>            labels);
	};
}
