// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <robofish/trackviewer/opengl/Program.hpp>
#include <robofish/trackviewer/opengl/Buffer.hpp>
#include <robofish/trackviewer/opengl/VertexArray.hpp>

namespace robofish::trackviewer
{
	class QuadRenderer
	{
		QuadRenderer(QuadRenderer const& src) = delete;
		QuadRenderer(QuadRenderer&& src)      = delete;
		QuadRenderer& operator=(QuadRenderer const& rhs) = delete;
		QuadRenderer& operator=(QuadRenderer&& rhs) = delete;

		Program     program_m;
		VertexArray vao_m;
		Buffer      vbo_m;

	public:
		QuadRenderer();

		void draw(glm::vec2 position, glm::vec2 size, glm::vec4 color, glm::mat4 const& world_to_clip);
	};
}
