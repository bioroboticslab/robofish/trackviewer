// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <epoxy/egl.h>

namespace robofish::trackviewer
{
	class HeadlessContext
	{
		HeadlessContext(HeadlessContext const& src) = delete;
		HeadlessContext(HeadlessContext&& src)      = delete;
		HeadlessContext& operator=(HeadlessContext const& rhs) = delete;
		HeadlessContext& operator=(HeadlessContext&& rhs) = delete;

		EGLDisplay display_m;
		EGLSurface surface_m;
		EGLContext context_m;

	public:
		HeadlessContext();
		virtual ~HeadlessContext();
		void make_current();
	};
}
