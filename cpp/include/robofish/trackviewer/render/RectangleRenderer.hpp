// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <robofish/trackviewer/opengl/Program.hpp>
#include <robofish/trackviewer/opengl/Buffer.hpp>
#include <robofish/trackviewer/opengl/VertexArray.hpp>

namespace robofish::trackviewer
{
	class RectangleRenderer
	{
		RectangleRenderer(RectangleRenderer const& src) = delete;
		RectangleRenderer(RectangleRenderer&& src)      = delete;
		RectangleRenderer& operator=(RectangleRenderer const& rhs) = delete;
		RectangleRenderer& operator=(RectangleRenderer&& rhs) = delete;

		Program     program_m;
		VertexArray vao_m;
		Buffer      vbo_m;

	public:
		RectangleRenderer();

		void draw(glm::vec2 position, glm::vec2 size, float thickness, glm::vec4 color, glm::mat4 const& world_to_clip);
	};
}
