// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <robofish/trackviewer/opengl/Program.hpp>
#include <robofish/trackviewer/opengl/VertexArray.hpp>
#include <robofish/trackviewer/opengl/Buffer.hpp>

#include <robofish/trackviewer/render/TextRenderer.hpp>

namespace robofish::trackviewer
{
	class AgentRenderer
	{
		AgentRenderer(AgentRenderer const& src) = delete;
		AgentRenderer(AgentRenderer&& src)      = delete;
		AgentRenderer& operator=(AgentRenderer const& rhs) = delete;
		AgentRenderer& operator=(AgentRenderer&& rhs) = delete;

		Program     program_m;
		VertexArray vao_m;
		Buffer      vbo_m;

	public:
		AgentRenderer();
		void draw(glm::vec4        pose,
		          float            radius,
		          float            outline_thickness,
		          float            tail_length,
		          glm::vec3        color,
		          glm::mat4 const& world_to_clip);
		void draw_with_label(glm::vec4        pose,
		                     std::string      label,
		                     float            label_min_radius,
		                     float            outline_thickness,
		                     float            tail_length,
		                     glm::vec3        color,
		                     glm::mat4 const& world_to_clip,
		                     TextRenderer&    text_renderer,
		                     float            px_to_world);
	};
}
