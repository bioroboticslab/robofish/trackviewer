// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <robofish/trackviewer/span.hpp>

#include <robofish/trackviewer/render/TextRenderer.hpp>
#include <robofish/trackviewer/render/RectangleRenderer.hpp>
#include <robofish/trackviewer/render/AgentRenderer.hpp>

#include <glm/vec4.hpp>
#include <glm/vec2.hpp>

namespace robofish::trackviewer
{
	class FrameRenderer
	{
		FrameRenderer() = delete;

		FrameRenderer(FrameRenderer const& src) = delete;
		FrameRenderer(FrameRenderer&& src)      = delete;
		FrameRenderer& operator=(FrameRenderer const& rhs) = delete;
		FrameRenderer& operator=(FrameRenderer&& rhs) = delete;

		float ppi_m;

		TextRenderer      text_renderer_m;
		RectangleRenderer rectangle_renderer_m;
		AgentRenderer     agent_renderer_m;

		void clear();

	public:
		FrameRenderer(float ppi);

		void setPixelsPerInch(float ppi);

		void draw(glm::uvec4                           viewport,
		          glm::vec2                            world_size,
		          stx::span<stx::span<const float, 4>> agent_poses,
		          stx::span<std::string>               agent_labels,
		          float                                agent_radius,
		          float                                agent_outline_thickness,
		          float                                agent_tail_length);
	};
}
