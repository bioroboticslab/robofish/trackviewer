// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <vector>
#include <unordered_map>
#include <string>
#include <memory>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <hb.h>
#include <hb-ft.h>

#include <robofish/trackviewer/opengl/Texture.hpp>

#include <robofish/trackviewer/opengl/Shader.hpp>
#include <robofish/trackviewer/opengl/Program.hpp>

#include <robofish/trackviewer/opengl/Buffer.hpp>
#include <robofish/trackviewer/opengl/VertexArray.hpp>

namespace robofish::trackviewer
{
	namespace freetype
	{
		struct library
		{
			FT_Library handle;

			operator FT_Library() const
			{
				return handle;
			}

			library(library const& src) = delete;
			library(library&& src)      = delete;
			library& operator=(library const& rhs) = delete;
			library& operator=(library&& rhs) = delete;

			library()
			{
				if (FT_Init_FreeType(&handle)) {
					throw std::runtime_error("Could not initialize FreeType library");
				}
			}

			~library()
			{
				FT_Done_FreeType(handle);
			}
		};

		struct face
		{
			FT_Face handle;

			operator FT_Face() const
			{
				return handle;
			}

			FT_Face operator->() const
			{
				return handle;
			}

			face(face const& src) = delete;
			face(face&& src)      = delete;
			face& operator=(face const& rhs) = delete;
			face& operator=(face&& rhs) = delete;

			face(library const& library, FT_Byte const* file_base, FT_Long file_size, FT_Long face_index)
			{
				if (FT_New_Memory_Face(library, file_base, file_size, face_index, &handle)) {
					throw std::runtime_error("Failed to load FreeType face");
				}
			}

			~face()
			{
				FT_Done_Face(handle);
			}
		};
	}

	class TextRenderer
	{

		struct Glyph : public Texture
		{
			glm::ivec2 size;
			glm::ivec2 bearing;

			Glyph(freetype::face const& face, char32_t codepoint);
		};

		Program shader_program_m;

		Buffer      glyph_vbo_m;
		VertexArray glyph_vao_m;

		std::unique_ptr<freetype::library>    ft_library_m;
		std::basic_string_view<unsigned char> ft_font_data_m;
		std::unique_ptr<freetype::face>       ft_face_m;

		std::unordered_map<char32_t, Glyph> rendered_glyphs_m;

		TextRenderer(TextRenderer const& src) = delete;
		TextRenderer(TextRenderer&& src)      = delete;
		TextRenderer& operator=(TextRenderer const& rhs) = delete;
		TextRenderer& operator=(TextRenderer&& rhs) = delete;

	public:
		TextRenderer();

		void setPixelsPerInch(float ppi);

		enum Align {
			Center = 0b0000,

			Left  = 0b0001,
			Right = 0b0010,

			Top    = 0b0100,
			Bottom = 0b1000,
		};

		auto draw(std::string const& text, glm::vec3 color, Align align, glm::mat4 const& model_to_clip)
		   -> std::tuple<float, float, float, float>;

	private:
		unsigned int height_m;
	};
}
