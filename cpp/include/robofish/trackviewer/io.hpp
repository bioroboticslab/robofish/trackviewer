// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <string>
#include <string_view>

namespace robofish::trackviewer
{
	std::basic_string_view<unsigned char> load_resource(std::string const& filename);
}
