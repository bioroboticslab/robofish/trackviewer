// SPDX-License-Identifier: GPL-3.0-or-later

#include <robofish/trackviewer/render/HeadlessContext.hpp>

#include <iomanip>
#include <sstream>
#include <unordered_set>
#include <string_view>
#include <stdexcept>
#include <vector>

#include <cstring>

namespace robofish::trackviewer
{
	std::string egl_error_msg()
	{
		std::ostringstream msg;
		msg << "EGL Error: 0x" << std::setw(4) << std::setfill('0') << std::hex << eglGetError();
		return msg.str();
	}

	std::unordered_set<std::string_view> string_list_to_set(char const* string_list)
	{
		std::unordered_set<std::string_view> extensions;
		size_t                               len;
		while ((len = std::strcspn(string_list, " "))) {
			extensions.emplace(string_list, len);
			string_list += len + 1;
		}
		return extensions;
	}

	template<typename T>
	constexpr bool version_ge(T version, int major, int minor)
	{
		return version[0] > major || version[0] == major && version[1] >= minor;
	}

	HeadlessContext::HeadlessContext()
	{
		display_m = eglGetDisplay(EGL_DEFAULT_DISPLAY);
		if (display_m == EGL_NO_DISPLAY) {
			throw std::runtime_error("No EGL display available");
		}

		EGLint version[2];
		if (!eglInitialize(display_m, &version[0], &version[1])) {
			throw std::runtime_error(egl_error_msg());
		}

		if (!version_ge(version, 1, 4)) {
			throw std::runtime_error("EGL 1.4 or greater required");
		}

		auto apis       = string_list_to_set(eglQueryString(display_m, EGL_CLIENT_APIS));
		auto extensions = string_list_to_set(eglQueryString(display_m, EGL_EXTENSIONS));

		std::vector<EGLConfig> gl_configs;
		if (apis.count("OpenGL")) {
			auto attribs = std::vector<EGLint>{EGL_SURFACE_TYPE,
			                                   EGL_PBUFFER_BIT,
			                                   EGL_BLUE_SIZE,
			                                   8,
			                                   EGL_GREEN_SIZE,
			                                   8,
			                                   EGL_RED_SIZE,
			                                   8,
			                                   EGL_DEPTH_SIZE,
			                                   8,
			                                   EGL_RENDERABLE_TYPE,
			                                   EGL_OPENGL_BIT,
			                                   EGL_NONE};

			EGLint num_configs;
			eglChooseConfig(display_m, attribs.data(), NULL, 0, &num_configs);
			gl_configs.resize(num_configs);
			eglChooseConfig(display_m, attribs.data(), gl_configs.data(), gl_configs.size(), &num_configs);
		}

		std::vector<EGLConfig> gles_configs;
		if (apis.count("OpenGL_ES")) {
			auto   attribs = std::vector<EGLint>{EGL_SURFACE_TYPE,
                                            EGL_PBUFFER_BIT,
                                            EGL_BLUE_SIZE,
                                            8,
                                            EGL_GREEN_SIZE,
                                            8,
                                            EGL_RED_SIZE,
                                            8,
                                            EGL_DEPTH_SIZE,
                                            8,
                                            EGL_RENDERABLE_TYPE,
                                            EGL_OPENGL_ES3_BIT,
                                            EGL_NONE};
			EGLint num_configs;
			eglChooseConfig(display_m, attribs.data(), NULL, 0, &num_configs);
			gles_configs.resize(num_configs);
			eglChooseConfig(display_m, attribs.data(), gles_configs.data(), gles_configs.size(), &num_configs);
		}

		if (gl_configs.size() == 0 && gles_configs.size() == 0) {
			throw std::runtime_error("No suitable EGL configurations available");
		}

		context_m            = EGL_NO_CONTEXT;
		EGLConfig egl_config = nullptr;
		if (context_m == EGL_NO_CONTEXT && eglBindAPI(EGL_OPENGL_API)
		    && (version_ge(version, 1, 5) || extensions.count("EGL_KHR_create_context"))) {
			auto attribs = std::vector<EGLint>{EGL_CONTEXT_MAJOR_VERSION,
			                                   4,
			                                   EGL_CONTEXT_MINOR_VERSION,
			                                   1,
			                                   EGL_CONTEXT_OPENGL_PROFILE_MASK,
			                                   EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
			                                   EGL_NONE};
			for (auto const& config : gl_configs) {
				context_m = eglCreateContext(display_m, config, EGL_NO_CONTEXT, attribs.data());
				if (context_m != EGL_NO_CONTEXT) {
					egl_config = config;
					break;
				}
			}
		}
		if (context_m == EGL_NO_CONTEXT && eglBindAPI(EGL_OPENGL_ES_API)
		    && (version_ge(version, 1, 5) || extensions.count("EGL_KHR_create_context"))) {
			auto attribs = std::vector<EGLint>{EGL_CONTEXT_MAJOR_VERSION, 3, EGL_CONTEXT_MINOR_VERSION, 0, EGL_NONE};
			for (auto const& config : gles_configs) {
				context_m = eglCreateContext(display_m, config, EGL_NO_CONTEXT, attribs.data());
				if (context_m != EGL_NO_CONTEXT) {
					egl_config = config;
					break;
				}
			}

			if (context_m == EGL_NO_CONTEXT) {
				throw std::runtime_error(egl_error_msg());
			}
		}

		if (context_m == EGL_NO_CONTEXT) {
			throw std::runtime_error(egl_error_msg());
		}

		if (extensions.count("EGL_KHR_surfaceless_context")) {
			surface_m = EGL_NO_SURFACE;
		} else {
			auto attribs = std::vector<EGLint>{EGL_WIDTH, 1, EGL_HEIGHT, 1, EGL_NONE};
			surface_m    = eglCreatePbufferSurface(display_m, egl_config, attribs.data());
			if (surface_m == EGL_NO_SURFACE) {
				throw std::runtime_error(egl_error_msg());
			}
		}
	}

	HeadlessContext::~HeadlessContext()
	{
		eglDestroyContext(display_m, context_m);
		if (surface_m != EGL_NO_SURFACE) {
			eglDestroySurface(display_m, surface_m);
		}
		eglTerminate(display_m);
	}

	void HeadlessContext::make_current()
	{
		if (!eglMakeCurrent(display_m, surface_m, surface_m, context_m)) {
			throw std::runtime_error(egl_error_msg());
		}
	}
}
