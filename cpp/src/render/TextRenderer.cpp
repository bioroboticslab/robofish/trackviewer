// SPDX-License-Identifier: GPL-3.0-or-later

#include <robofish/trackviewer/render/TextRenderer.hpp>

#include <robofish/trackviewer/io.hpp>

#include <epoxy/gl.h>

#include <limits>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <iomanip>

namespace robofish::trackviewer
{
	TextRenderer::Glyph::Glyph(freetype::face const& face, char32_t codepoint)
	: Texture(GL_TEXTURE_2D)
	{
		if (FT_Load_Glyph(face, codepoint, FT_LOAD_RENDER)) {
			std::cerr << "Failed to load glyph for codepoint U+" << std::setfill('0') << std::setw(4) << std::hex
			          << static_cast<int32_t>(codepoint) << std::endl;
			return;
		}

		GLint unpack_alignment;
		glGetIntegerv(GL_UNPACK_ALIGNMENT, &unpack_alignment);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glBindTexture(GL_TEXTURE_2D, *this);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glPixelStorei(GL_UNPACK_ALIGNMENT, unpack_alignment);

		size    = {face->glyph->bitmap.width, face->glyph->bitmap.rows};
		bearing = {face->glyph->bitmap_left, face->glyph->bitmap_top};
	}

	TextRenderer::TextRenderer()
	: shader_program_m(Shader{GL_VERTEX_SHADER, load_resource("shader/text.vert")},
	                   Shader{GL_FRAGMENT_SHADER, load_resource("shader/text.frag")})
	, ft_library_m(new freetype::library())
	, ft_font_data_m(load_resource("font/IBMPlexMono-Regular.otf"))
	, ft_face_m(new freetype::face(*ft_library_m,
	                               reinterpret_cast<FT_Byte const*>(&ft_font_data_m[0]),
	                               ft_font_data_m.size(),
	                               0))
	, height_m(12)
	{
		glBindVertexArray(glyph_vao_m);

		const auto vertex_attribute = glGetAttribLocation(shader_program_m, "vertex");
		glEnableVertexAttribArray(vertex_attribute);

		// NOTE: One quad consists of two triangles with three four-dimensional
		// float vertices
		glBindBuffer(GL_ARRAY_BUFFER, glyph_vbo_m);
		glBufferData(GL_ARRAY_BUFFER, 2 * 3 * 4 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(vertex_attribute, 4, GL_FLOAT, GL_FALSE, 0, 0);

		setPixelsPerInch(96);
	}

	void TextRenderer::setPixelsPerInch(float ppi)
	{
		FT_Set_Char_Size(*ft_face_m, 0, static_cast<FT_UInt>(height_m * 64), ppi, ppi);
	}

	auto TextRenderer::draw(std::string const& text, glm::vec3 color, Align align, glm::mat4 const& model_to_clip)
	   -> std::tuple<float, float, float, float>
	{
		// Determine bounding box around text
		auto font = hb_ft_font_create(*ft_face_m, NULL);

		auto buffer = hb_buffer_create();
		hb_buffer_add_utf8(buffer, &text[0], text.size(), 0, text.size());

		hb_buffer_guess_segment_properties(buffer);

		hb_shape(font, buffer, NULL, 0);

		auto glyph_count     = hb_buffer_get_length(buffer);
		auto glyph_info      = hb_buffer_get_glyph_infos(buffer, NULL);
		auto glyph_positions = hb_buffer_get_glyph_positions(buffer, NULL);

		auto min_x = 0.f;
		auto min_y = 0.f;
		auto max_x = 0.f;
		auto max_y = 0.f;
		{
			auto cursor_x = 0.f;
			auto cursor_y = 0.f;
			for (decltype(glyph_count) i = 0; i < glyph_count; ++i) {
				const auto codepoint = glyph_info[i].codepoint;

				rendered_glyphs_m.try_emplace(codepoint, *ft_face_m, codepoint);
				decltype(auto) glyph = rendered_glyphs_m.at(codepoint);

				const auto x_offset  = glyph_positions[i].x_offset / 64.0f;
				const auto y_offset  = glyph_positions[i].y_offset / 64.0f;
				const auto x_advance = glyph_positions[i].x_advance / 64.0f;
				const auto y_advance = glyph_positions[i].y_advance / 64.0f;

				const auto xpos = cursor_x + x_offset + glyph.bearing.x;
				const auto ypos = cursor_y + y_offset - (glyph.size.y - glyph.bearing.y);

				const auto w = glyph.size.x;
				const auto h = glyph.size.y;

				min_x = std::min(min_x, xpos);
				min_y = std::min(min_y, ypos);
				max_x = std::max(max_x, xpos + w);
				max_y = std::max(max_y, ypos + h);

				cursor_x += x_advance;
				cursor_y += y_advance;
			}
		}

		auto cursor_x = [&]() {
			if ((align & Left) == Left)
				return 0.f;
			else if ((align & Right) == Right)
				return -(max_x - min_x);
			else
				return -(max_x - min_x) / 2;
		}();
		auto cursor_y = [&]() {
			if ((align & Bottom) == Bottom)
				return 0.f;
			if ((align & Top) == Top)
				return -(max_y - min_y);
			else
				return -(max_y - min_y) / 2;
		}();
		min_x += cursor_x;
		max_x += cursor_x;
		min_y += cursor_y;
		max_y += cursor_y;

		// Draw text in a 1:1 glyph <-> texture mapping

		glUseProgram(shader_program_m);
		glUniformMatrix4fv(glGetUniformLocation(shader_program_m, "model_to_clip"), 1, GL_FALSE, &model_to_clip[0][0]);

		glUniform3f(glGetUniformLocation(shader_program_m, "text_color"), color.x, color.y, color.z);
		glBindVertexArray(glyph_vao_m);

		for (decltype(glyph_count) i = 0; i < glyph_count; ++i) {
			auto codepoint = glyph_info[i].codepoint;

			rendered_glyphs_m.try_emplace(codepoint, *ft_face_m, codepoint);
			decltype(auto) glyph = rendered_glyphs_m.at(codepoint);

			const auto x_offset  = glyph_positions[i].x_offset / 64.0f;
			const auto y_offset  = glyph_positions[i].y_offset / 64.0f;
			const auto x_advance = glyph_positions[i].x_advance / 64.0f;
			const auto y_advance = glyph_positions[i].y_advance / 64.0f;

			const auto xpos = cursor_x + x_offset + glyph.bearing.x;
			const auto ypos = cursor_y + y_offset - (glyph.size.y - glyph.bearing.y);

			const auto w = glyph.size.x;
			const auto h = glyph.size.y;

			GLfloat vertices[6][4] = {{xpos, ypos + h, 0.0, 0.0},
			                          {xpos, ypos, 0.0, 1.0},
			                          {xpos + w, ypos, 1.0, 1.0},

			                          {xpos, ypos + h, 0.0, 0.0},
			                          {xpos + w, ypos, 1.0, 1.0},
			                          {xpos + w, ypos + h, 1.0, 0.0}};

			glBindTexture(GL_TEXTURE_2D, glyph);

			glBindBuffer(GL_ARRAY_BUFFER, glyph_vbo_m);
			glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), &vertices[0]);

			glDrawArrays(GL_TRIANGLES, 0, 6);

			cursor_x += x_advance;
			cursor_y += y_advance;
		}

		hb_buffer_destroy(buffer);
		hb_font_destroy(font);

		glBindTexture(GL_TEXTURE_2D, 0);

		return {min_x, min_y, max_x, max_y};
	}
}
