// SPDX-License-Identifier: GPL-3.0-or-later

#include <array>

#include <epoxy/gl.h>

#include <robofish/trackviewer/opengl/Shader.hpp>
#include <robofish/trackviewer/render/QuadRenderer.hpp>

#include <robofish/trackviewer/io.hpp>

namespace robofish::trackviewer
{
	constexpr static std::size_t num_vertices = 4;

	QuadRenderer::QuadRenderer()
	: program_m{Shader{GL_VERTEX_SHADER, load_resource("shader/default.vert")},
	            Shader{GL_FRAGMENT_SHADER, load_resource("shader/default.frag")}}
	{
		glBindVertexArray(vao_m);

		const auto position_attribute = glGetAttribLocation(program_m, "position");
		glEnableVertexAttribArray(position_attribute);

		glBindBuffer(GL_ARRAY_BUFFER, vbo_m);
		glBufferData(GL_ARRAY_BUFFER, num_vertices * sizeof(glm::vec4), NULL, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(position_attribute, 4, GL_FLOAT, GL_FALSE, 0, 0);
	}

	void QuadRenderer::draw(glm::vec2 position, glm::vec2 size, glm::vec4 color, glm::mat4 const& world_to_clip)
	{
		glm::vec4 const tl{position.x, position.y, 0, 1};
		glm::vec4 const tr{position.x + size.x, position.y, 0, 1};
		glm::vec4 const bl{position.x, position.y + size.y, 0, 1};
		glm::vec4 const br{position.x + size.x, position.y + size.y, 0, 1};

		std::array vertices = {tl, bl, tr, br};

		glBindBuffer(GL_ARRAY_BUFFER, vbo_m);
		glBufferSubData(GL_ARRAY_BUFFER, 0, num_vertices * sizeof(glm::vec4), &vertices[0]);

		glUseProgram(program_m);
		glBindVertexArray(vao_m);
		glUniformMatrix4fv(glGetUniformLocation(program_m, "model_to_clip"), 1, GL_FALSE, &world_to_clip[0][0]);
		glUniform4fv(glGetUniformLocation(program_m, "color"), 1, &color[0]);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, num_vertices);
	}
}
