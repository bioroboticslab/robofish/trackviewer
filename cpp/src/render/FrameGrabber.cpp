// SPDX-License-Identifier: GPL-3.0-or-later

#include <robofish/trackviewer/render/FrameGrabber.hpp>

namespace robofish::trackviewer
{
	FrameGrabber::FrameGrabber(int width, int height)
	: width_m(width)
	, height_m(height)
	, framebuffer_m(width, height, 1)
	{
	}

	int FrameGrabber::frame_size() const
	{
		return frame_size_m;
	}

	void FrameGrabber::grab(unsigned char* frame)
	{
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer_m);
		glBlitFramebuffer(0, 0, width_m, height_m, 0, 0, width_m, height_m, GL_COLOR_BUFFER_BIT, GL_NEAREST);

		glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer_m);
		glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
		glReadPixels(0, 0, width_m, height_m, GL_BGRA, GL_UNSIGNED_BYTE, frame);
	}
}
