// SPDX-License-Identifier: GPL-3.0-or-later

#include <robofish/trackviewer/render/ViewVectorsRenderer.hpp>

#include <cmath>

#include <epoxy/gl.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

namespace robofish::trackviewer
{
	ViewVectorsRenderer::ViewVectorsRenderer(float ppi)
	: ppcm_m{ppi / 2.54f}
	{
		text_renderer_m.setPixelsPerInch(ppi);
	}

	void ViewVectorsRenderer::setPixelsPerInch(float ppi)
	{
		ppcm_m = ppi / 2.54f;
		text_renderer_m.setPixelsPerInch(ppi);
	}

	void ViewVectorsRenderer::draw(glm::uvec4                        viewport,
	                               stx::span<stx::span<const float>> view_vectors,
	                               float                             outline_thickness,
	                               glm::vec2                         size,
	                               glm::vec2                         margins,
	                               glm::vec2                         spacing,
	                               stx::span<std::string>            labels)
	{
		glEnable(GL_SCISSOR_TEST);

		glDisable(GL_DEPTH_TEST);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glViewport(viewport.x, viewport.y, viewport.z, viewport.w);
		glScissor(viewport.x, viewport.y, viewport.z, viewport.w);

		glClearColor(1, 1, 1, 1);
		glClearDepth(1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glDisable(GL_SCISSOR_TEST);

		const auto world_to_clip
		   = glm::scale(glm::vec3{1, -1, 1})
		     * glm::ortho(0.f, static_cast<float>(viewport.z), 0.f, static_cast<float>(viewport.w), 0.f, 2.f)
		     * glm::lookAt(glm::vec3{0, 0, 1}, glm::vec3{0, 0, 0}, glm::vec3{0, 1, 0});

		const auto cm_to_px = ppcm_m;

		const auto color                = glm::vec4{0, 0, 0, 1};
		const auto outline_thickness_px = cm_to_px * outline_thickness;

		const auto size_px = size * cm_to_px;

		const auto cell_size  = size_px;
		const auto margins_px = cm_to_px * margins;
		const auto spacing_px = cm_to_px * spacing;

		const auto cell_space = glm::vec2{viewport.z - 2 * margins_px.x - cell_size.x,
		                                  viewport.w - 2 * margins_px.y - cell_size.y};
		if (cell_space.x <= 0 || cell_space.y <= 0) {
			return;
		}

		auto num_horizontal_cells = 1 + static_cast<std::size_t>(std::floor(cell_space.x / (cell_size.x + spacing_px.x)));
		auto num_vertical_cells   = 1 + static_cast<std::size_t>(std::floor(cell_space.y / (cell_size.y + spacing_px.y)));
		const auto num_cells      = num_horizontal_cells * num_vertical_cells;

		const auto initial_position = margins_px;

		std::size_t cell_index = 0;
		for (decltype(auto) segments : view_vectors) {
			if (cell_index >= num_cells) {
				break;
			}

			const auto cell_index_x = std::floor(cell_index / num_vertical_cells);
			const auto cell_index_y = cell_index % num_vertical_cells;

			auto position = glm::vec2{
			   initial_position.x + cell_index_x * (cell_size.x + (cell_index_x ? spacing_px.x : 0)),
			   initial_position.y + cell_index_y * (cell_size.y + (cell_index_y ? spacing_px.y : 0))};

			auto adjusted_cell_size = cell_size;
			if (cell_index < labels.size()) {
				decltype(auto) label          = labels[cell_index];
				const auto     origin_to_clip = world_to_clip
				                            * glm::translate(
				                               glm::vec3{position.x, position.y + adjusted_cell_size.y / 2, 0});
				auto [min_x, min_y, max_x, max_y] = text_renderer_m.draw(label,
				                                                         glm::vec4{0.f, 0.f, 0.f, 1.f},
				                                                         TextRenderer::Center,
				                                                         origin_to_clip * glm::scale(glm::vec3{1, -1, 1}));
				const auto stolen_width           = max_x - min_x + cm_to_px * 0.1;
				position.x += stolen_width;
				adjusted_cell_size.x -= stolen_width;
			}

			rectangle_renderer_m.draw(position, adjusted_cell_size, outline_thickness_px, color, world_to_clip);
			const auto segment_size = glm::vec2{(adjusted_cell_size.x - outline_thickness_px) / segments.size(),
			                                    adjusted_cell_size.y - outline_thickness_px};
			for (std::size_t i = 0; i < segments.size(); ++i) {
				auto segment_position = position + outline_thickness_px / 2.f;
				segment_position.x += i * segment_size.x;
				const auto segment_color = glm::vec4{1.f - segments[i], 1.f - segments[i], 1.f - segments[i], 1};
				quad_renderer_m.draw(segment_position, segment_size, segment_color, world_to_clip);
			}

			++cell_index;
		}
	}
}
