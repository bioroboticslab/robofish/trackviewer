// SPDX-License-Identifier: GPL-3.0-or-later

#include <stdexcept>

#include <robofish/trackviewer/io.hpp>

#include "shader_default_vertex.hpp"
#include "shader_default_fragment.hpp"
#include "shader_text_vertex.hpp"
#include "shader_text_fragment.hpp"

#include "font_ibm_plex_mono_regular_data.hpp"
#include "font_ibm_plex_mono_regular_license.hpp"

#include <iostream>
#include <fstream>

namespace robofish::trackviewer
{
	std::basic_string_view<unsigned char> load_resource(std::string const& filename)
	{
		if (filename == "shader/default.vert") {
			return resource_shader_default_vertex();
		}

		if (filename == "shader/default.frag") {
			return resource_shader_default_fragment();
		}

		if (filename == "shader/text.vert") {
			return resource_shader_text_vertex();
		}

		if (filename == "shader/text.frag") {
			return resource_shader_text_fragment();
		}

		if (filename == "font/IBMPlexMono-Regular.otf") {
			return resource_font_ibm_plex_mono_regular_data();
		}

		if (filename == "font/license.txt") {
			return resource_font_ibm_plex_mono_regular_license();
		}

		throw std::invalid_argument(filename + ": No such file or directory");
	}
}
