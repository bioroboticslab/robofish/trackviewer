// SPDX-License-Identifier: GPL-3.0-or-later
#version 410 core

uniform mat4 model_to_clip;

in vec4 vertex;
out vec2 TexCoords;


void main()
{
    gl_Position = model_to_clip * vec4(vertex.xy, 0.0, 1.0);
    TexCoords = vertex.zw;
}
