// SPDX-License-Identifier: GPL-3.0-or-later
#version 410 core

in vec2 TexCoords;
out vec4 color;

uniform sampler2D text;
uniform vec3 text_color;

void main()
{
    color = vec4(text_color, texture(text, TexCoords).r);
}
