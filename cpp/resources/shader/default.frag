// SPDX-License-Identifier: GPL-3.0-or-later
#version 410 core

out vec4 f_color;

uniform vec4 color;

void main()
{
    f_color = color;
}
