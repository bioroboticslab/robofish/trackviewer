# SPDX-License-Identifier: GPL-3.0-or-later


function(locate_shared_library_anyof VAR)
    cmake_parse_arguments(ARG "NO_SUFFIX" "" "" ${ARGN})
    message(STATUS "Locating one of ${ARG_UNPARSED_ARGUMENTS}")
    foreach(name ${ARG_UNPARSED_ARGUMENTS})
        set(_filename "${CMAKE_SHARED_LIBRARY_PREFIX}${name}")
        if(NOT ARG_NO_SUFFIX)
            set(_filename "${_filename}${CMAKE_SHARED_LIBRARY_SUFFIX}")
        endif()
        if("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
            execute_process(
                COMMAND where.exe "${_filename}"
                OUTPUT_VARIABLE _matches
                OUTPUT_STRIP_TRAILING_WHITESPACE
                RESULT_VARIABLE _res
            )
            if("${_res}" STREQUAL "0")
                string(REGEX REPLACE "\n" ";" _matches "${_matches}")
                list(GET _matches 0 _match)
                string(REGEX REPLACE "\\\\" "/" _match "${_match}")

                if(DEFINED ${VAR})
                    list(APPEND ${VAR} "${_match}")
                    set(${VAR} ${${VAR}} PARENT_SCOPE)
                else()
                    set(${VAR} "${_match}" PARENT_SCOPE)
                endif()
                return()
            endif()
        elseif("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
            execute_process(
                    COMMAND /sbin/ldconfig -p
                    OUTPUT_VARIABLE _cache
                    OUTPUT_STRIP_TRAILING_WHITESPACE
                    RESULT_VARIABLE _res
            )
            if("${_res}" STREQUAL "0")
                string(REPLACE "." "\\." _regex_filename "${_filename}")
                string(REGEX MATCH "=> ((/[^/\n]*)*${_regex_filename})" _matches "${_cache}")

                if(NOT "${CMAKE_MATCH_1}" STREQUAL "")
                    set(_match "${CMAKE_MATCH_1}")

                    if(DEFINED ${VAR})
                        list(APPEND ${VAR} "${_match}")
                        set(${VAR} ${${VAR}} PARENT_SCOPE)
                    else()
                        set(${VAR} "${_match}" PARENT_SCOPE)
                    endif()
                    return()
                endif()
            endif()
        else()
            message(FATAL_ERROR "Platform not supported")
        endif()
    endforeach()
    message(FATAL_ERROR "Could not locate any of ${ARGN}")
endfunction()

function(locate_shared_library VAR)
    cmake_parse_arguments(ARG "NO_SUFFIX" "" "" ${ARGN})
    foreach(name ${ARG_UNPARSED_ARGUMENTS})
        set(_filename "${CMAKE_SHARED_LIBRARY_PREFIX}${name}")
        if(NOT ARG_NO_SUFFIX)
            set(_filename "${_filename}${CMAKE_SHARED_LIBRARY_SUFFIX}")
        endif()
        message(STATUS "Locating ${_filename}")
        if("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
            execute_process(
                COMMAND where.exe "${_filename}"
                OUTPUT_VARIABLE _matches
                OUTPUT_STRIP_TRAILING_WHITESPACE
                RESULT_VARIABLE _res
            )
            if(NOT "${_res}" STREQUAL "0")
                message(FATAL_ERROR "Could not locate ${_filename}")
            endif()
            string(REGEX REPLACE "\n" ";" _matches "${_matches}")
            list(GET _matches 0 _match)
            string(REGEX REPLACE "\\\\" "/" _match "${_match}")
        elseif("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
            execute_process(
                    COMMAND /sbin/ldconfig -p
                    OUTPUT_VARIABLE _cache
                    OUTPUT_STRIP_TRAILING_WHITESPACE
                    RESULT_VARIABLE _res
            )
            if(NOT "${_res}" STREQUAL "0")
                message(FATAL_ERROR "Could not locate ${_filename}")
            endif()
            string(REPLACE "." "\\." _regex_filename "${_filename}")
            string(REGEX MATCH "=> ((/[^/\n]*)*${_regex_filename})" _matches "${_cache}")
            if("${CMAKE_MATCH_1}" STREQUAL "")
                message(FATAL_ERROR "Could not locate ${_filename}")
            endif()
            set(_match "${CMAKE_MATCH_1}")
        else()
            message(FATAL_ERROR "Platform not supported")
        endif()

        if(DEFINED ${VAR})
            list(APPEND ${VAR} "${_match}")
            set(${VAR} ${${VAR}} PARENT_SCOPE)
        else()
            set(${VAR} "${_match}" PARENT_SCOPE)
        endif()
    endforeach()
endfunction()
