# SPDX-License-Identifier: GPL-3.0-or-later

function(add_resource_library name)
	cmake_parse_arguments(ARG "" "" "RESOURCES" ${ARGN})

	set(RESOURCE_DIR "${CMAKE_CURRENT_BINARY_DIR}/${name}")

	file(MAKE_DIRECTORY "${RESOURCE_DIR}")

	add_library(${name} STATIC EXCLUDE_FROM_ALL)
	set_target_properties(${name} PROPERTIES PREFIX "")

	target_include_directories(${name} PUBLIC
		$<BUILD_INTERFACE:${RESOURCE_DIR}>
		$<INSTALL_INTERFACE:${INSTALL_INCLUDEDIR}>
	)

	set_target_properties(${name} PROPERTIES
		CXX_STANDARD 17
		CXX_STANDARD_REQUIRED YES
		CXX_EXTENSIONS NO
	)

	set_target_properties(${name} PROPERTIES POSITION_INDEPENDENT_CODE ON)

	file(WRITE "${RESOURCE_DIR}/Generate.cmake"
"# SPDX-License-Identifier: GPL-3.0-or-later

set(IDENTIFIER \"\${CMAKE_ARGV3}\")
set(FILE_NAME \"\${CMAKE_ARGV4}\")

file(SIZE \"\${FILE_NAME}\" FILE_SIZE)
file(READ \"\${FILE_NAME}\" FILE_CONTENT_ENCODED HEX)
math(EXPR FILE_CONTENT_LENGTH \"\${FILE_SIZE} * 2\")

file(WRITE \"${RESOURCE_DIR}/\${IDENTIFIER}.hpp\"
\"// SPDX-License-Identifier: GPL-3.0-or-later

#include <string_view>

extern const unsigned char g_resource_\${IDENTIFIER}[\${FILE_SIZE}];

constexpr std::basic_string_view<unsigned char> resource_\${IDENTIFIER}() { return {g_resource_\${IDENTIFIER}, \${FILE_SIZE}}; }
\")

set(POS 0)
set(BYTES)
while(\${POS} LESS \${FILE_CONTENT_LENGTH})
	string(SUBSTRING \"\${FILE_CONTENT_ENCODED}\" \${POS} 2 BYTE)
	string(APPEND BYTES \"0x\${BYTE},\\n\")
	math(EXPR POS \"\${POS} + 2\")
endwhile()

file(WRITE \"${RESOURCE_DIR}/\${IDENTIFIER}.cpp\"
\"// SPDX-License-Identifier: GPL-3.0-or-later

extern const unsigned char g_resource_\${IDENTIFIER}[\${FILE_SIZE}] = {
\${BYTES}};
\")
")
	target_resources(${name} RESOURCES ${ARG_RESOURCES})
endfunction()

function(target_resources target)
	cmake_parse_arguments(ARG "" "" "RESOURCES" ${ARGN})

	list(LENGTH ARG_RESOURCES STOP)
	math(EXPR STOP "${STOP} - 1")

	foreach(IDENTIFIER_INDEX RANGE 0 ${STOP} 2)
		list(GET ARG_RESOURCES ${IDENTIFIER_INDEX} IDENTIFIER)
		math(EXPR FILE_NAME_INDEX "${IDENTIFIER_INDEX} + 1")
		list(GET ARG_RESOURCES ${FILE_NAME_INDEX} FILE_NAME)
		generate_resource(${target} IDENTIFIER ${IDENTIFIER} FILE_NAME ${FILE_NAME})
	endforeach()
endfunction()

function(generate_resource target)
	cmake_parse_arguments(ARG "" "IDENTIFIER;FILE_NAME" "" ${ARGN})

	set(RESOURCE_DIR "${CMAKE_CURRENT_BINARY_DIR}/${target}")

	get_filename_component(ARG_FILE_NAME "${ARG_FILE_NAME}" ABSOLUTE)

	add_custom_command(
		OUTPUT "${RESOURCE_DIR}/${ARG_IDENTIFIER}.hpp" "${RESOURCE_DIR}/${ARG_IDENTIFIER}.cpp"
		COMMAND ${CMAKE_COMMAND} -P "${RESOURCE_DIR}/Generate.cmake" "${ARG_IDENTIFIER}" "${ARG_FILE_NAME}"
		DEPENDS "${ARG_FILE_NAME}"
	)

	target_sources(${target} PRIVATE "${RESOURCE_DIR}/${ARG_IDENTIFIER}.hpp" "${RESOURCE_DIR}/${ARG_IDENTIFIER}.cpp")

endfunction()
